﻿namespace Project
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.gBoxPreview = new System.Windows.Forms.GroupBox();
            this.vlc16 = new AxAXVLC.AxVLCPlugin2();
            this.vlc15 = new AxAXVLC.AxVLCPlugin2();
            this.vlc14 = new AxAXVLC.AxVLCPlugin2();
            this.vlc13 = new AxAXVLC.AxVLCPlugin2();
            this.vlc12 = new AxAXVLC.AxVLCPlugin2();
            this.vlc11 = new AxAXVLC.AxVLCPlugin2();
            this.vlc10 = new AxAXVLC.AxVLCPlugin2();
            this.vlc9 = new AxAXVLC.AxVLCPlugin2();
            this.vlc8 = new AxAXVLC.AxVLCPlugin2();
            this.vlc7 = new AxAXVLC.AxVLCPlugin2();
            this.vlc6 = new AxAXVLC.AxVLCPlugin2();
            this.vlc5 = new AxAXVLC.AxVLCPlugin2();
            this.vlc4 = new AxAXVLC.AxVLCPlugin2();
            this.vlc3 = new AxAXVLC.AxVLCPlugin2();
            this.vlc2 = new AxAXVLC.AxVLCPlugin2();
            this.vlc1 = new AxAXVLC.AxVLCPlugin2();
            this.gBoxLive = new System.Windows.Forms.GroupBox();
            this.imageBox5 = new Emgu.CV.UI.ImageBox();
            this.vlcLive = new AxAXVLC.AxVLCPlugin2();
            this.btnPrev1 = new System.Windows.Forms.Button();
            this.gBoxSource = new System.Windows.Forms.GroupBox();
            this.btnRemoveSource = new System.Windows.Forms.Button();
            this.btnAddSource = new System.Windows.Forms.Button();
            this.lstSource = new System.Windows.Forms.ListView();
            this.Titles = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.gBoxCamera = new System.Windows.Forms.GroupBox();
            this.imageBox4 = new Emgu.CV.UI.ImageBox();
            this.imageBox3 = new Emgu.CV.UI.ImageBox();
            this.imageBox2 = new Emgu.CV.UI.ImageBox();
            this.imageBox1 = new Emgu.CV.UI.ImageBox();
            this.btnCam1 = new System.Windows.Forms.Button();
            this.btnPrev2 = new System.Windows.Forms.Button();
            this.btnPrev3 = new System.Windows.Forms.Button();
            this.btnPrev4 = new System.Windows.Forms.Button();
            this.gBoxButtons = new System.Windows.Forms.GroupBox();
            this.btnPopUp = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btnLive = new System.Windows.Forms.Button();
            this.btnLogo = new System.Windows.Forms.Button();
            this.btnPlaylist = new System.Windows.Forms.Button();
            this.btnCam4 = new System.Windows.Forms.Button();
            this.btnCam3 = new System.Windows.Forms.Button();
            this.btnCam2 = new System.Windows.Forms.Button();
            this.btnPrev16 = new System.Windows.Forms.Button();
            this.btnPrev15 = new System.Windows.Forms.Button();
            this.btnPrev14 = new System.Windows.Forms.Button();
            this.btnPrev13 = new System.Windows.Forms.Button();
            this.btnPrev12 = new System.Windows.Forms.Button();
            this.btnPrev11 = new System.Windows.Forms.Button();
            this.btnPrev10 = new System.Windows.Forms.Button();
            this.btnPrev9 = new System.Windows.Forms.Button();
            this.btnPrev8 = new System.Windows.Forms.Button();
            this.btnPrev7 = new System.Windows.Forms.Button();
            this.btnPrev6 = new System.Windows.Forms.Button();
            this.btnPrev5 = new System.Windows.Forms.Button();
            this.gBoxPlaylist = new System.Windows.Forms.GroupBox();
            this.btnRemoveVideo = new System.Windows.Forms.Button();
            this.btnAddVideo = new System.Windows.Forms.Button();
            this.lstPlaylist = new System.Windows.Forms.ListView();
            this.lblTitle = new System.Windows.Forms.Label();
            this.gBoxCameraSource = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lstCamera = new System.Windows.Forms.ListView();
            this.btnSourceToPlayList = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.streamToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.titleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainLogoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemDark = new System.Windows.Forms.ToolStripMenuItem();
            this.themeLight = new System.Windows.Forms.ToolStripMenuItem();
            this.themeDark = new System.Windows.Forms.ToolStripMenuItem();
            this.btnPlaylistToSource = new System.Windows.Forms.Button();
            this.picBoxMainLogo = new System.Windows.Forms.PictureBox();
            this.gBoxPreview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vlc16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc1)).BeginInit();
            this.gBoxLive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlcLive)).BeginInit();
            this.gBoxSource.SuspendLayout();
            this.gBoxCamera.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).BeginInit();
            this.gBoxButtons.SuspendLayout();
            this.gBoxPlaylist.SuspendLayout();
            this.gBoxCameraSource.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxMainLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // gBoxPreview
            // 
            this.gBoxPreview.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gBoxPreview.BackColor = System.Drawing.Color.Transparent;
            this.gBoxPreview.Controls.Add(this.vlc16);
            this.gBoxPreview.Controls.Add(this.vlc15);
            this.gBoxPreview.Controls.Add(this.vlc14);
            this.gBoxPreview.Controls.Add(this.vlc13);
            this.gBoxPreview.Controls.Add(this.vlc12);
            this.gBoxPreview.Controls.Add(this.vlc11);
            this.gBoxPreview.Controls.Add(this.vlc10);
            this.gBoxPreview.Controls.Add(this.vlc9);
            this.gBoxPreview.Controls.Add(this.vlc8);
            this.gBoxPreview.Controls.Add(this.vlc7);
            this.gBoxPreview.Controls.Add(this.vlc6);
            this.gBoxPreview.Controls.Add(this.vlc5);
            this.gBoxPreview.Controls.Add(this.vlc4);
            this.gBoxPreview.Controls.Add(this.vlc3);
            this.gBoxPreview.Controls.Add(this.vlc2);
            this.gBoxPreview.Controls.Add(this.vlc1);
            this.gBoxPreview.Location = new System.Drawing.Point(974, 37);
            this.gBoxPreview.Name = "gBoxPreview";
            this.gBoxPreview.Size = new System.Drawing.Size(919, 606);
            this.gBoxPreview.TabIndex = 0;
            this.gBoxPreview.TabStop = false;
            this.gBoxPreview.Text = "Preview";
            // 
            // vlc16
            // 
            this.vlc16.Enabled = true;
            this.vlc16.Location = new System.Drawing.Point(690, 456);
            this.vlc16.Name = "vlc16";
            this.vlc16.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc16.OcxState")));
            this.vlc16.Size = new System.Drawing.Size(220, 139);
            this.vlc16.TabIndex = 16;
            // 
            // vlc15
            // 
            this.vlc15.Enabled = true;
            this.vlc15.Location = new System.Drawing.Point(463, 457);
            this.vlc15.Name = "vlc15";
            this.vlc15.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc15.OcxState")));
            this.vlc15.Size = new System.Drawing.Size(222, 139);
            this.vlc15.TabIndex = 15;
            // 
            // vlc14
            // 
            this.vlc14.Enabled = true;
            this.vlc14.Location = new System.Drawing.Point(235, 457);
            this.vlc14.Name = "vlc14";
            this.vlc14.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc14.OcxState")));
            this.vlc14.Size = new System.Drawing.Size(222, 139);
            this.vlc14.TabIndex = 14;
            // 
            // vlc13
            // 
            this.vlc13.Enabled = true;
            this.vlc13.Location = new System.Drawing.Point(7, 457);
            this.vlc13.Name = "vlc13";
            this.vlc13.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc13.OcxState")));
            this.vlc13.Size = new System.Drawing.Size(222, 139);
            this.vlc13.TabIndex = 13;
            // 
            // vlc12
            // 
            this.vlc12.Enabled = true;
            this.vlc12.Location = new System.Drawing.Point(690, 311);
            this.vlc12.Name = "vlc12";
            this.vlc12.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc12.OcxState")));
            this.vlc12.Size = new System.Drawing.Size(222, 139);
            this.vlc12.TabIndex = 12;
            // 
            // vlc11
            // 
            this.vlc11.Enabled = true;
            this.vlc11.Location = new System.Drawing.Point(462, 310);
            this.vlc11.Name = "vlc11";
            this.vlc11.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc11.OcxState")));
            this.vlc11.Size = new System.Drawing.Size(222, 139);
            this.vlc11.TabIndex = 11;
            // 
            // vlc10
            // 
            this.vlc10.Enabled = true;
            this.vlc10.Location = new System.Drawing.Point(234, 311);
            this.vlc10.Name = "vlc10";
            this.vlc10.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc10.OcxState")));
            this.vlc10.Size = new System.Drawing.Size(222, 139);
            this.vlc10.TabIndex = 10;
            // 
            // vlc9
            // 
            this.vlc9.Enabled = true;
            this.vlc9.Location = new System.Drawing.Point(6, 311);
            this.vlc9.Name = "vlc9";
            this.vlc9.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc9.OcxState")));
            this.vlc9.Size = new System.Drawing.Size(222, 139);
            this.vlc9.TabIndex = 9;
            // 
            // vlc8
            // 
            this.vlc8.Enabled = true;
            this.vlc8.Location = new System.Drawing.Point(690, 165);
            this.vlc8.Name = "vlc8";
            this.vlc8.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc8.OcxState")));
            this.vlc8.Size = new System.Drawing.Size(222, 139);
            this.vlc8.TabIndex = 8;
            // 
            // vlc7
            // 
            this.vlc7.Enabled = true;
            this.vlc7.Location = new System.Drawing.Point(462, 165);
            this.vlc7.Name = "vlc7";
            this.vlc7.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc7.OcxState")));
            this.vlc7.Size = new System.Drawing.Size(222, 139);
            this.vlc7.TabIndex = 7;
            // 
            // vlc6
            // 
            this.vlc6.Enabled = true;
            this.vlc6.Location = new System.Drawing.Point(234, 165);
            this.vlc6.Name = "vlc6";
            this.vlc6.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc6.OcxState")));
            this.vlc6.Size = new System.Drawing.Size(222, 139);
            this.vlc6.TabIndex = 6;
            // 
            // vlc5
            // 
            this.vlc5.Enabled = true;
            this.vlc5.Location = new System.Drawing.Point(6, 165);
            this.vlc5.Name = "vlc5";
            this.vlc5.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc5.OcxState")));
            this.vlc5.Size = new System.Drawing.Size(222, 139);
            this.vlc5.TabIndex = 5;
            // 
            // vlc4
            // 
            this.vlc4.Enabled = true;
            this.vlc4.Location = new System.Drawing.Point(689, 19);
            this.vlc4.Name = "vlc4";
            this.vlc4.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc4.OcxState")));
            this.vlc4.Size = new System.Drawing.Size(222, 139);
            this.vlc4.TabIndex = 4;
            // 
            // vlc3
            // 
            this.vlc3.Enabled = true;
            this.vlc3.Location = new System.Drawing.Point(461, 19);
            this.vlc3.Name = "vlc3";
            this.vlc3.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc3.OcxState")));
            this.vlc3.Size = new System.Drawing.Size(222, 139);
            this.vlc3.TabIndex = 3;
            // 
            // vlc2
            // 
            this.vlc2.Enabled = true;
            this.vlc2.Location = new System.Drawing.Point(233, 19);
            this.vlc2.Name = "vlc2";
            this.vlc2.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc2.OcxState")));
            this.vlc2.Size = new System.Drawing.Size(222, 139);
            this.vlc2.TabIndex = 2;
            // 
            // vlc1
            // 
            this.vlc1.Enabled = true;
            this.vlc1.Location = new System.Drawing.Point(6, 19);
            this.vlc1.Name = "vlc1";
            this.vlc1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlc1.OcxState")));
            this.vlc1.Size = new System.Drawing.Size(220, 139);
            this.vlc1.TabIndex = 1;
            // 
            // gBoxLive
            // 
            this.gBoxLive.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gBoxLive.BackColor = System.Drawing.Color.Transparent;
            this.gBoxLive.Controls.Add(this.imageBox5);
            this.gBoxLive.Controls.Add(this.vlcLive);
            this.gBoxLive.Location = new System.Drawing.Point(1331, 647);
            this.gBoxLive.Name = "gBoxLive";
            this.gBoxLive.Size = new System.Drawing.Size(562, 385);
            this.gBoxLive.TabIndex = 1;
            this.gBoxLive.TabStop = false;
            this.gBoxLive.Text = "Live";
            // 
            // imageBox5
            // 
            this.imageBox5.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox5.Location = new System.Drawing.Point(6, 19);
            this.imageBox5.Name = "imageBox5";
            this.imageBox5.Size = new System.Drawing.Size(548, 360);
            this.imageBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageBox5.TabIndex = 6;
            this.imageBox5.TabStop = false;
            this.imageBox5.Visible = false;
            // 
            // vlcLive
            // 
            this.vlcLive.Enabled = true;
            this.vlcLive.Location = new System.Drawing.Point(6, 19);
            this.vlcLive.Name = "vlcLive";
            this.vlcLive.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("vlcLive.OcxState")));
            this.vlcLive.Size = new System.Drawing.Size(547, 360);
            this.vlcLive.TabIndex = 0;
            // 
            // btnPrev1
            // 
            this.btnPrev1.Location = new System.Drawing.Point(6, 19);
            this.btnPrev1.Name = "btnPrev1";
            this.btnPrev1.Size = new System.Drawing.Size(105, 65);
            this.btnPrev1.TabIndex = 2;
            this.btnPrev1.Text = "Preview1";
            this.btnPrev1.UseVisualStyleBackColor = true;
            this.btnPrev1.Click += new System.EventHandler(this.button1_Click);
            // 
            // gBoxSource
            // 
            this.gBoxSource.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gBoxSource.BackColor = System.Drawing.Color.Transparent;
            this.gBoxSource.Controls.Add(this.btnRemoveSource);
            this.gBoxSource.Controls.Add(this.btnAddSource);
            this.gBoxSource.Controls.Add(this.lstSource);
            this.gBoxSource.Location = new System.Drawing.Point(731, 37);
            this.gBoxSource.Name = "gBoxSource";
            this.gBoxSource.Size = new System.Drawing.Size(226, 606);
            this.gBoxSource.TabIndex = 3;
            this.gBoxSource.TabStop = false;
            this.gBoxSource.Text = "Source";
            // 
            // btnRemoveSource
            // 
            this.btnRemoveSource.Location = new System.Drawing.Point(7, 561);
            this.btnRemoveSource.Name = "btnRemoveSource";
            this.btnRemoveSource.Size = new System.Drawing.Size(89, 39);
            this.btnRemoveSource.TabIndex = 3;
            this.btnRemoveSource.Text = "Remove";
            this.btnRemoveSource.UseVisualStyleBackColor = true;
            this.btnRemoveSource.Click += new System.EventHandler(this.BtnRemoveSource_Click);
            // 
            // btnAddSource
            // 
            this.btnAddSource.Location = new System.Drawing.Point(131, 561);
            this.btnAddSource.Name = "btnAddSource";
            this.btnAddSource.Size = new System.Drawing.Size(89, 39);
            this.btnAddSource.TabIndex = 2;
            this.btnAddSource.Text = "Add Source";
            this.btnAddSource.UseVisualStyleBackColor = true;
            this.btnAddSource.Click += new System.EventHandler(this.button2_Click);
            // 
            // lstSource
            // 
            this.lstSource.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Titles});
            this.lstSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstSource.HideSelection = false;
            this.lstSource.Location = new System.Drawing.Point(5, 19);
            this.lstSource.MultiSelect = false;
            this.lstSource.Name = "lstSource";
            this.lstSource.Size = new System.Drawing.Size(215, 521);
            this.lstSource.TabIndex = 1;
            this.lstSource.UseCompatibleStateImageBehavior = false;
            // 
            // gBoxCamera
            // 
            this.gBoxCamera.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gBoxCamera.BackColor = System.Drawing.Color.Transparent;
            this.gBoxCamera.Controls.Add(this.imageBox4);
            this.gBoxCamera.Controls.Add(this.imageBox3);
            this.gBoxCamera.Controls.Add(this.imageBox2);
            this.gBoxCamera.Controls.Add(this.imageBox1);
            this.gBoxCamera.Location = new System.Drawing.Point(13, 647);
            this.gBoxCamera.Name = "gBoxCamera";
            this.gBoxCamera.Size = new System.Drawing.Size(427, 385);
            this.gBoxCamera.TabIndex = 4;
            this.gBoxCamera.TabStop = false;
            this.gBoxCamera.Text = "Camera";
            // 
            // imageBox4
            // 
            this.imageBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox4.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox4.Location = new System.Drawing.Point(221, 213);
            this.imageBox4.Name = "imageBox4";
            this.imageBox4.Size = new System.Drawing.Size(200, 166);
            this.imageBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageBox4.TabIndex = 5;
            this.imageBox4.TabStop = false;
            // 
            // imageBox3
            // 
            this.imageBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox3.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox3.Location = new System.Drawing.Point(6, 213);
            this.imageBox3.Name = "imageBox3";
            this.imageBox3.Size = new System.Drawing.Size(201, 166);
            this.imageBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageBox3.TabIndex = 4;
            this.imageBox3.TabStop = false;
            // 
            // imageBox2
            // 
            this.imageBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox2.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox2.Location = new System.Drawing.Point(220, 19);
            this.imageBox2.Name = "imageBox2";
            this.imageBox2.Size = new System.Drawing.Size(201, 167);
            this.imageBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageBox2.TabIndex = 3;
            this.imageBox2.TabStop = false;
            // 
            // imageBox1
            // 
            this.imageBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.imageBox1.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.imageBox1.Location = new System.Drawing.Point(7, 20);
            this.imageBox1.Name = "imageBox1";
            this.imageBox1.Size = new System.Drawing.Size(201, 166);
            this.imageBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageBox1.TabIndex = 2;
            this.imageBox1.TabStop = false;
            // 
            // btnCam1
            // 
            this.btnCam1.Location = new System.Drawing.Point(6, 314);
            this.btnCam1.Name = "btnCam1";
            this.btnCam1.Size = new System.Drawing.Size(105, 65);
            this.btnCam1.TabIndex = 5;
            this.btnCam1.Text = "Camara1";
            this.btnCam1.UseVisualStyleBackColor = true;
            this.btnCam1.Click += new System.EventHandler(this.Button3_Click);
            // 
            // btnPrev2
            // 
            this.btnPrev2.Location = new System.Drawing.Point(117, 19);
            this.btnPrev2.Name = "btnPrev2";
            this.btnPrev2.Size = new System.Drawing.Size(105, 65);
            this.btnPrev2.TabIndex = 7;
            this.btnPrev2.Text = "Preview2";
            this.btnPrev2.UseVisualStyleBackColor = true;
            this.btnPrev2.Click += new System.EventHandler(this.btnPrev2_Click);
            // 
            // btnPrev3
            // 
            this.btnPrev3.Location = new System.Drawing.Point(228, 19);
            this.btnPrev3.Name = "btnPrev3";
            this.btnPrev3.Size = new System.Drawing.Size(105, 65);
            this.btnPrev3.TabIndex = 8;
            this.btnPrev3.Text = "Preview3";
            this.btnPrev3.UseVisualStyleBackColor = true;
            this.btnPrev3.Click += new System.EventHandler(this.btnPrev3_Click);
            // 
            // btnPrev4
            // 
            this.btnPrev4.Location = new System.Drawing.Point(339, 19);
            this.btnPrev4.Name = "btnPrev4";
            this.btnPrev4.Size = new System.Drawing.Size(105, 65);
            this.btnPrev4.TabIndex = 9;
            this.btnPrev4.Text = "Preview4";
            this.btnPrev4.UseVisualStyleBackColor = true;
            this.btnPrev4.Click += new System.EventHandler(this.btnPrev4_Click);
            // 
            // gBoxButtons
            // 
            this.gBoxButtons.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gBoxButtons.BackColor = System.Drawing.Color.Transparent;
            this.gBoxButtons.Controls.Add(this.btnPopUp);
            this.gBoxButtons.Controls.Add(this.button5);
            this.gBoxButtons.Controls.Add(this.btnLive);
            this.gBoxButtons.Controls.Add(this.btnLogo);
            this.gBoxButtons.Controls.Add(this.btnPlaylist);
            this.gBoxButtons.Controls.Add(this.btnCam4);
            this.gBoxButtons.Controls.Add(this.btnCam3);
            this.gBoxButtons.Controls.Add(this.btnCam2);
            this.gBoxButtons.Controls.Add(this.btnPrev16);
            this.gBoxButtons.Controls.Add(this.btnPrev15);
            this.gBoxButtons.Controls.Add(this.btnCam1);
            this.gBoxButtons.Controls.Add(this.btnPrev14);
            this.gBoxButtons.Controls.Add(this.btnPrev13);
            this.gBoxButtons.Controls.Add(this.btnPrev12);
            this.gBoxButtons.Controls.Add(this.btnPrev11);
            this.gBoxButtons.Controls.Add(this.btnPrev10);
            this.gBoxButtons.Controls.Add(this.btnPrev9);
            this.gBoxButtons.Controls.Add(this.btnPrev8);
            this.gBoxButtons.Controls.Add(this.btnPrev7);
            this.gBoxButtons.Controls.Add(this.btnPrev6);
            this.gBoxButtons.Controls.Add(this.btnPrev5);
            this.gBoxButtons.Controls.Add(this.btnPrev1);
            this.gBoxButtons.Controls.Add(this.btnPrev2);
            this.gBoxButtons.Controls.Add(this.btnPrev4);
            this.gBoxButtons.Controls.Add(this.btnPrev3);
            this.gBoxButtons.Location = new System.Drawing.Point(721, 647);
            this.gBoxButtons.Name = "gBoxButtons";
            this.gBoxButtons.Size = new System.Drawing.Size(578, 385);
            this.gBoxButtons.TabIndex = 10;
            this.gBoxButtons.TabStop = false;
            this.gBoxButtons.Text = "Control Button";
            // 
            // btnPopUp
            // 
            this.btnPopUp.Location = new System.Drawing.Point(467, 20);
            this.btnPopUp.Name = "btnPopUp";
            this.btnPopUp.Size = new System.Drawing.Size(105, 65);
            this.btnPopUp.TabIndex = 29;
            this.btnPopUp.Text = "Live Pop-Up";
            this.btnPopUp.UseVisualStyleBackColor = true;
            this.btnPopUp.Click += new System.EventHandler(this.btnPopUp_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(467, 232);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(105, 65);
            this.button5.TabIndex = 28;
            this.button5.Text = "Go Offline";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // btnLive
            // 
            this.btnLive.Location = new System.Drawing.Point(467, 161);
            this.btnLive.Name = "btnLive";
            this.btnLive.Size = new System.Drawing.Size(105, 65);
            this.btnLive.TabIndex = 27;
            this.btnLive.Text = "Go Live";
            this.btnLive.UseVisualStyleBackColor = true;
            this.btnLive.Click += new System.EventHandler(this.btnLive_Click);
            // 
            // btnLogo
            // 
            this.btnLogo.Location = new System.Drawing.Point(467, 90);
            this.btnLogo.Name = "btnLogo";
            this.btnLogo.Size = new System.Drawing.Size(105, 65);
            this.btnLogo.TabIndex = 26;
            this.btnLogo.Text = "Set Logo";
            this.btnLogo.UseVisualStyleBackColor = true;
            this.btnLogo.Click += new System.EventHandler(this.btnLogo_Click);
            // 
            // btnPlaylist
            // 
            this.btnPlaylist.Location = new System.Drawing.Point(467, 314);
            this.btnPlaylist.Name = "btnPlaylist";
            this.btnPlaylist.Size = new System.Drawing.Size(105, 65);
            this.btnPlaylist.TabIndex = 25;
            this.btnPlaylist.Text = "PlayList";
            this.btnPlaylist.UseVisualStyleBackColor = true;
            this.btnPlaylist.Click += new System.EventHandler(this.btnPlaylist_Click);
            // 
            // btnCam4
            // 
            this.btnCam4.Location = new System.Drawing.Point(339, 314);
            this.btnCam4.Name = "btnCam4";
            this.btnCam4.Size = new System.Drawing.Size(105, 65);
            this.btnCam4.TabIndex = 24;
            this.btnCam4.Text = "Camara4";
            this.btnCam4.UseVisualStyleBackColor = true;
            this.btnCam4.Click += new System.EventHandler(this.BtnCam4_Click);
            // 
            // btnCam3
            // 
            this.btnCam3.Location = new System.Drawing.Point(228, 314);
            this.btnCam3.Name = "btnCam3";
            this.btnCam3.Size = new System.Drawing.Size(105, 65);
            this.btnCam3.TabIndex = 23;
            this.btnCam3.Text = "Camara3";
            this.btnCam3.UseVisualStyleBackColor = true;
            this.btnCam3.Click += new System.EventHandler(this.BtnCam3_Click);
            // 
            // btnCam2
            // 
            this.btnCam2.Location = new System.Drawing.Point(117, 314);
            this.btnCam2.Name = "btnCam2";
            this.btnCam2.Size = new System.Drawing.Size(105, 65);
            this.btnCam2.TabIndex = 22;
            this.btnCam2.Text = "Camara2";
            this.btnCam2.UseVisualStyleBackColor = true;
            this.btnCam2.Click += new System.EventHandler(this.BtnCam2_Click);
            // 
            // btnPrev16
            // 
            this.btnPrev16.Location = new System.Drawing.Point(339, 232);
            this.btnPrev16.Name = "btnPrev16";
            this.btnPrev16.Size = new System.Drawing.Size(105, 65);
            this.btnPrev16.TabIndex = 21;
            this.btnPrev16.Text = "Preview16";
            this.btnPrev16.UseVisualStyleBackColor = true;
            this.btnPrev16.Click += new System.EventHandler(this.btnPrev16_Click);
            // 
            // btnPrev15
            // 
            this.btnPrev15.Location = new System.Drawing.Point(228, 232);
            this.btnPrev15.Name = "btnPrev15";
            this.btnPrev15.Size = new System.Drawing.Size(105, 65);
            this.btnPrev15.TabIndex = 20;
            this.btnPrev15.Text = "Preview15";
            this.btnPrev15.UseVisualStyleBackColor = true;
            this.btnPrev15.Click += new System.EventHandler(this.btnPrev15_Click);
            // 
            // btnPrev14
            // 
            this.btnPrev14.Location = new System.Drawing.Point(117, 232);
            this.btnPrev14.Name = "btnPrev14";
            this.btnPrev14.Size = new System.Drawing.Size(105, 65);
            this.btnPrev14.TabIndex = 19;
            this.btnPrev14.Text = "Preview14";
            this.btnPrev14.UseVisualStyleBackColor = true;
            this.btnPrev14.Click += new System.EventHandler(this.btnPrev14_Click);
            // 
            // btnPrev13
            // 
            this.btnPrev13.Location = new System.Drawing.Point(7, 232);
            this.btnPrev13.Name = "btnPrev13";
            this.btnPrev13.Size = new System.Drawing.Size(105, 65);
            this.btnPrev13.TabIndex = 18;
            this.btnPrev13.Text = "Preview13";
            this.btnPrev13.UseVisualStyleBackColor = true;
            this.btnPrev13.Click += new System.EventHandler(this.btnPrev13_Click);
            // 
            // btnPrev12
            // 
            this.btnPrev12.Location = new System.Drawing.Point(339, 161);
            this.btnPrev12.Name = "btnPrev12";
            this.btnPrev12.Size = new System.Drawing.Size(105, 65);
            this.btnPrev12.TabIndex = 17;
            this.btnPrev12.Text = "Preview12";
            this.btnPrev12.UseVisualStyleBackColor = true;
            this.btnPrev12.Click += new System.EventHandler(this.btnPrev12_Click);
            // 
            // btnPrev11
            // 
            this.btnPrev11.Location = new System.Drawing.Point(228, 161);
            this.btnPrev11.Name = "btnPrev11";
            this.btnPrev11.Size = new System.Drawing.Size(105, 65);
            this.btnPrev11.TabIndex = 16;
            this.btnPrev11.Text = "Preview11";
            this.btnPrev11.UseVisualStyleBackColor = true;
            this.btnPrev11.Click += new System.EventHandler(this.btnPrev11_Click);
            // 
            // btnPrev10
            // 
            this.btnPrev10.Location = new System.Drawing.Point(118, 161);
            this.btnPrev10.Name = "btnPrev10";
            this.btnPrev10.Size = new System.Drawing.Size(105, 65);
            this.btnPrev10.TabIndex = 15;
            this.btnPrev10.Text = "Preview10";
            this.btnPrev10.UseVisualStyleBackColor = true;
            this.btnPrev10.Click += new System.EventHandler(this.btnPrev10_Click);
            // 
            // btnPrev9
            // 
            this.btnPrev9.Location = new System.Drawing.Point(7, 161);
            this.btnPrev9.Name = "btnPrev9";
            this.btnPrev9.Size = new System.Drawing.Size(105, 65);
            this.btnPrev9.TabIndex = 14;
            this.btnPrev9.Text = "Preview9";
            this.btnPrev9.UseVisualStyleBackColor = true;
            this.btnPrev9.Click += new System.EventHandler(this.btnPrev9_Click);
            // 
            // btnPrev8
            // 
            this.btnPrev8.Location = new System.Drawing.Point(339, 90);
            this.btnPrev8.Name = "btnPrev8";
            this.btnPrev8.Size = new System.Drawing.Size(105, 65);
            this.btnPrev8.TabIndex = 13;
            this.btnPrev8.Text = "Preview8";
            this.btnPrev8.UseVisualStyleBackColor = true;
            this.btnPrev8.Click += new System.EventHandler(this.btnPrev8_Click);
            // 
            // btnPrev7
            // 
            this.btnPrev7.Location = new System.Drawing.Point(228, 90);
            this.btnPrev7.Name = "btnPrev7";
            this.btnPrev7.Size = new System.Drawing.Size(105, 65);
            this.btnPrev7.TabIndex = 12;
            this.btnPrev7.Text = "Preview7";
            this.btnPrev7.UseVisualStyleBackColor = true;
            this.btnPrev7.Click += new System.EventHandler(this.btnPrev7_Click);
            // 
            // btnPrev6
            // 
            this.btnPrev6.Location = new System.Drawing.Point(117, 90);
            this.btnPrev6.Name = "btnPrev6";
            this.btnPrev6.Size = new System.Drawing.Size(105, 65);
            this.btnPrev6.TabIndex = 11;
            this.btnPrev6.Text = "Preview6";
            this.btnPrev6.UseVisualStyleBackColor = true;
            this.btnPrev6.Click += new System.EventHandler(this.btnPrev6_Click);
            // 
            // btnPrev5
            // 
            this.btnPrev5.Location = new System.Drawing.Point(6, 90);
            this.btnPrev5.Name = "btnPrev5";
            this.btnPrev5.Size = new System.Drawing.Size(105, 65);
            this.btnPrev5.TabIndex = 10;
            this.btnPrev5.Text = "Preview5";
            this.btnPrev5.UseVisualStyleBackColor = true;
            this.btnPrev5.Click += new System.EventHandler(this.btnPrev5_Click);
            // 
            // gBoxPlaylist
            // 
            this.gBoxPlaylist.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gBoxPlaylist.BackColor = System.Drawing.Color.Transparent;
            this.gBoxPlaylist.Controls.Add(this.btnRemoveVideo);
            this.gBoxPlaylist.Controls.Add(this.btnAddVideo);
            this.gBoxPlaylist.Controls.Add(this.lstPlaylist);
            this.gBoxPlaylist.Location = new System.Drawing.Point(457, 37);
            this.gBoxPlaylist.Name = "gBoxPlaylist";
            this.gBoxPlaylist.Size = new System.Drawing.Size(200, 606);
            this.gBoxPlaylist.TabIndex = 11;
            this.gBoxPlaylist.TabStop = false;
            this.gBoxPlaylist.Text = "Playlist";
            // 
            // btnRemoveVideo
            // 
            this.btnRemoveVideo.Location = new System.Drawing.Point(9, 561);
            this.btnRemoveVideo.Name = "btnRemoveVideo";
            this.btnRemoveVideo.Size = new System.Drawing.Size(90, 39);
            this.btnRemoveVideo.TabIndex = 4;
            this.btnRemoveVideo.Text = "Clear";
            this.btnRemoveVideo.UseVisualStyleBackColor = true;
            this.btnRemoveVideo.Click += new System.EventHandler(this.BtnRemoveVideo_Click);
            // 
            // btnAddVideo
            // 
            this.btnAddVideo.Location = new System.Drawing.Point(105, 561);
            this.btnAddVideo.Name = "btnAddVideo";
            this.btnAddVideo.Size = new System.Drawing.Size(89, 39);
            this.btnAddVideo.TabIndex = 3;
            this.btnAddVideo.Text = "Add Video";
            this.btnAddVideo.UseVisualStyleBackColor = true;
            this.btnAddVideo.Click += new System.EventHandler(this.btnAddVideo_Click);
            // 
            // lstPlaylist
            // 
            this.lstPlaylist.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstPlaylist.HideSelection = false;
            this.lstPlaylist.Location = new System.Drawing.Point(6, 19);
            this.lstPlaylist.MultiSelect = false;
            this.lstPlaylist.Name = "lstPlaylist";
            this.lstPlaylist.Size = new System.Drawing.Size(188, 521);
            this.lstPlaylist.TabIndex = 0;
            this.lstPlaylist.UseCompatibleStateImageBehavior = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(13, 35);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(427, 224);
            this.lblTitle.TabIndex = 12;
            this.lblTitle.Text = "Junta de Freguesia de   Rio Tinto";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // gBoxCameraSource
            // 
            this.gBoxCameraSource.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gBoxCameraSource.BackColor = System.Drawing.Color.Transparent;
            this.gBoxCameraSource.Controls.Add(this.button1);
            this.gBoxCameraSource.Controls.Add(this.lstCamera);
            this.gBoxCameraSource.Location = new System.Drawing.Point(457, 647);
            this.gBoxCameraSource.Name = "gBoxCameraSource";
            this.gBoxCameraSource.Size = new System.Drawing.Size(258, 385);
            this.gBoxCameraSource.TabIndex = 13;
            this.gBoxCameraSource.TabStop = false;
            this.gBoxCameraSource.Text = "Camera Source";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 340);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(246, 39);
            this.button1.TabIndex = 4;
            this.button1.Text = "Refresh Cameras";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // lstCamera
            // 
            this.lstCamera.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstCamera.HideSelection = false;
            this.lstCamera.Location = new System.Drawing.Point(6, 23);
            this.lstCamera.MultiSelect = false;
            this.lstCamera.Name = "lstCamera";
            this.lstCamera.Size = new System.Drawing.Size(246, 291);
            this.lstCamera.TabIndex = 0;
            this.lstCamera.UseCompatibleStateImageBehavior = false;
            // 
            // btnSourceToPlayList
            // 
            this.btnSourceToPlayList.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSourceToPlayList.Location = new System.Drawing.Point(663, 200);
            this.btnSourceToPlayList.Name = "btnSourceToPlayList";
            this.btnSourceToPlayList.Size = new System.Drawing.Size(62, 41);
            this.btnSourceToPlayList.TabIndex = 15;
            this.btnSourceToPlayList.Text = "<<";
            this.btnSourceToPlayList.UseVisualStyleBackColor = true;
            this.btnSourceToPlayList.Click += new System.EventHandler(this.btnSourceToPlayList_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.White;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.streamToolStripMenuItem,
            this.windowToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1904, 24);
            this.menuStrip1.TabIndex = 16;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // streamToolStripMenuItem
            // 
            this.streamToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurationToolStripMenuItem});
            this.streamToolStripMenuItem.Name = "streamToolStripMenuItem";
            this.streamToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.streamToolStripMenuItem.Text = "Livestream";
            // 
            // configurationToolStripMenuItem
            // 
            this.configurationToolStripMenuItem.Name = "configurationToolStripMenuItem";
            this.configurationToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.configurationToolStripMenuItem.Text = "Configuration";
            this.configurationToolStripMenuItem.Click += new System.EventHandler(this.configurationToolStripMenuItem_Click);
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.titleToolStripMenuItem,
            this.mainLogoToolStripMenuItem,
            this.menuItemDark});
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.windowToolStripMenuItem.Text = "Customize";
            // 
            // titleToolStripMenuItem
            // 
            this.titleToolStripMenuItem.Name = "titleToolStripMenuItem";
            this.titleToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.titleToolStripMenuItem.Text = "Title";
            this.titleToolStripMenuItem.Click += new System.EventHandler(this.titleToolStripMenuItem_Click);
            // 
            // mainLogoToolStripMenuItem
            // 
            this.mainLogoToolStripMenuItem.Name = "mainLogoToolStripMenuItem";
            this.mainLogoToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.mainLogoToolStripMenuItem.Text = "Main Logo";
            this.mainLogoToolStripMenuItem.Click += new System.EventHandler(this.mainLogoToolStripMenuItem_Click);
            // 
            // menuItemDark
            // 
            this.menuItemDark.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.themeLight,
            this.themeDark});
            this.menuItemDark.Name = "menuItemDark";
            this.menuItemDark.Size = new System.Drawing.Size(131, 22);
            this.menuItemDark.Text = "Theme";
            // 
            // themeLight
            // 
            this.themeLight.Checked = true;
            this.themeLight.CheckState = System.Windows.Forms.CheckState.Checked;
            this.themeLight.Name = "themeLight";
            this.themeLight.Size = new System.Drawing.Size(101, 22);
            this.themeLight.Text = "Light";
            this.themeLight.Click += new System.EventHandler(this.themeLight_Click);
            // 
            // themeDark
            // 
            this.themeDark.Name = "themeDark";
            this.themeDark.Size = new System.Drawing.Size(101, 22);
            this.themeDark.Text = "Dark";
            this.themeDark.Click += new System.EventHandler(this.themeDark_Click);
            // 
            // btnPlaylistToSource
            // 
            this.btnPlaylistToSource.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPlaylistToSource.Location = new System.Drawing.Point(663, 387);
            this.btnPlaylistToSource.Name = "btnPlaylistToSource";
            this.btnPlaylistToSource.Size = new System.Drawing.Size(62, 41);
            this.btnPlaylistToSource.TabIndex = 18;
            this.btnPlaylistToSource.Text = ">>";
            this.btnPlaylistToSource.UseVisualStyleBackColor = true;
            this.btnPlaylistToSource.Click += new System.EventHandler(this.btnPlaylistToSource_Click);
            // 
            // picBoxMainLogo
            // 
            this.picBoxMainLogo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.picBoxMainLogo.BackColor = System.Drawing.Color.Transparent;
            this.picBoxMainLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBoxMainLogo.Image = global::Project.Properties.Resources.rt;
            this.picBoxMainLogo.Location = new System.Drawing.Point(13, 264);
            this.picBoxMainLogo.Name = "picBoxMainLogo";
            this.picBoxMainLogo.Size = new System.Drawing.Size(427, 368);
            this.picBoxMainLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxMainLogo.TabIndex = 19;
            this.picBoxMainLogo.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1904, 1041);
            this.Controls.Add(this.picBoxMainLogo);
            this.Controls.Add(this.btnPlaylistToSource);
            this.Controls.Add(this.btnSourceToPlayList);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.gBoxCameraSource);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.gBoxPlaylist);
            this.Controls.Add(this.gBoxButtons);
            this.Controls.Add(this.gBoxCamera);
            this.Controls.Add(this.gBoxSource);
            this.Controls.Add(this.gBoxLive);
            this.Controls.Add(this.gBoxPreview);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Video";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.gBoxPreview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vlc16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlc1)).EndInit();
            this.gBoxLive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vlcLive)).EndInit();
            this.gBoxSource.ResumeLayout(false);
            this.gBoxCamera.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imageBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).EndInit();
            this.gBoxButtons.ResumeLayout(false);
            this.gBoxPlaylist.ResumeLayout(false);
            this.gBoxCameraSource.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxMainLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gBoxPreview;
        private System.Windows.Forms.GroupBox gBoxLive;
        private System.Windows.Forms.Button btnPrev1;
        private AxAXVLC.AxVLCPlugin2 vlc1;
        private AxAXVLC.AxVLCPlugin2 vlcLive;
        private AxAXVLC.AxVLCPlugin2 vlc15;
        private AxAXVLC.AxVLCPlugin2 vlc14;
        private AxAXVLC.AxVLCPlugin2 vlc13;
        private AxAXVLC.AxVLCPlugin2 vlc12;
        private AxAXVLC.AxVLCPlugin2 vlc11;
        private AxAXVLC.AxVLCPlugin2 vlc10;
        private AxAXVLC.AxVLCPlugin2 vlc9;
        private AxAXVLC.AxVLCPlugin2 vlc8;
        private AxAXVLC.AxVLCPlugin2 vlc7;
        private AxAXVLC.AxVLCPlugin2 vlc6;
        private AxAXVLC.AxVLCPlugin2 vlc5;
        private AxAXVLC.AxVLCPlugin2 vlc4;
        private AxAXVLC.AxVLCPlugin2 vlc3;
        private AxAXVLC.AxVLCPlugin2 vlc2;
        private AxAXVLC.AxVLCPlugin2 vlc16;
        private System.Windows.Forms.GroupBox gBoxSource;
        private System.Windows.Forms.ListView lstSource;
        private System.Windows.Forms.Button btnAddSource;
        private System.Windows.Forms.GroupBox gBoxCamera;
        private Emgu.CV.UI.ImageBox imageBox1;
        private System.Windows.Forms.Button btnCam1;
        private System.Windows.Forms.ColumnHeader Titles;
        private System.Windows.Forms.Button btnPrev2;
        private System.Windows.Forms.Button btnPrev3;
        private System.Windows.Forms.Button btnPrev4;
        private System.Windows.Forms.GroupBox gBoxButtons;
        private System.Windows.Forms.Button btnPrev16;
        private System.Windows.Forms.Button btnPrev15;
        private System.Windows.Forms.Button btnPrev14;
        private System.Windows.Forms.Button btnPrev13;
        private System.Windows.Forms.Button btnPrev12;
        private System.Windows.Forms.Button btnPrev11;
        private System.Windows.Forms.Button btnPrev10;
        private System.Windows.Forms.Button btnPrev9;
        private System.Windows.Forms.Button btnPrev8;
        private System.Windows.Forms.Button btnPrev7;
        private System.Windows.Forms.Button btnPrev6;
        private System.Windows.Forms.Button btnPrev5;
        private System.Windows.Forms.Button btnCam4;
        private System.Windows.Forms.Button btnCam3;
        private System.Windows.Forms.Button btnCam2;
        private Emgu.CV.UI.ImageBox imageBox4;
        private Emgu.CV.UI.ImageBox imageBox3;
        private Emgu.CV.UI.ImageBox imageBox2;
        private System.Windows.Forms.Button btnRemoveSource;
        private System.Windows.Forms.Button btnPopUp;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btnLive;
        private System.Windows.Forms.Button btnLogo;
        private System.Windows.Forms.Button btnPlaylist;
        private System.Windows.Forms.GroupBox gBoxPlaylist;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.GroupBox gBoxCameraSource;
        private System.Windows.Forms.Button btnSourceToPlayList;
        private System.Windows.Forms.ListView lstPlaylist;
        private System.Windows.Forms.ListView lstCamera;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuItemDark;
        private System.Windows.Forms.Button btnRemoveVideo;
        private System.Windows.Forms.Button btnAddVideo;
        private System.Windows.Forms.ToolStripMenuItem titleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mainLogoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem themeLight;
        private System.Windows.Forms.ToolStripMenuItem themeDark;
        private System.Windows.Forms.ToolStripMenuItem streamToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurationToolStripMenuItem;
        private System.Windows.Forms.Button btnPlaylistToSource;
        private Emgu.CV.UI.ImageBox imageBox5;
        private System.Windows.Forms.PictureBox picBoxMainLogo;
        private System.Windows.Forms.Button button1;
    }
}

