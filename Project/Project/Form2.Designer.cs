﻿namespace Project
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbType = new System.Windows.Forms.ComboBox();
            this.lblSource = new System.Windows.Forms.Label();
            this.gBoxYT = new System.Windows.Forms.GroupBox();
            this.txtYTURL = new System.Windows.Forms.TextBox();
            this.txtYTTile = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gBoxIP = new System.Windows.Forms.GroupBox();
            this.txtIPIp = new System.Windows.Forms.TextBox();
            this.txtIPTitle = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.gBoxLF = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnLFBrowse = new System.Windows.Forms.Button();
            this.txtLFPath = new System.Windows.Forms.TextBox();
            this.txtLFTitle = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.gBoxYT.SuspendLayout();
            this.gBoxIP.SuspendLayout();
            this.gBoxLF.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbType
            // 
            this.cbType.BackColor = System.Drawing.Color.Gainsboro;
            this.cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "YouTube",
            "LocalFile",
            "IP"});
            this.cbType.Location = new System.Drawing.Point(148, 20);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(203, 21);
            this.cbType.TabIndex = 0;
            this.cbType.SelectedIndexChanged += new System.EventHandler(this.cbType_SelectedIndexChanged);
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = true;
            this.lblSource.BackColor = System.Drawing.Color.Transparent;
            this.lblSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSource.Location = new System.Drawing.Point(50, 21);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(92, 16);
            this.lblSource.TabIndex = 1;
            this.lblSource.Text = "Source Type: ";
            // 
            // gBoxYT
            // 
            this.gBoxYT.BackColor = System.Drawing.Color.Transparent;
            this.gBoxYT.Controls.Add(this.txtYTURL);
            this.gBoxYT.Controls.Add(this.txtYTTile);
            this.gBoxYT.Controls.Add(this.label3);
            this.gBoxYT.Controls.Add(this.label2);
            this.gBoxYT.Location = new System.Drawing.Point(12, 58);
            this.gBoxYT.Name = "gBoxYT";
            this.gBoxYT.Size = new System.Drawing.Size(385, 100);
            this.gBoxYT.TabIndex = 3;
            this.gBoxYT.TabStop = false;
            this.gBoxYT.Text = "YouTube";
            // 
            // txtYTURL
            // 
            this.txtYTURL.Location = new System.Drawing.Point(52, 56);
            this.txtYTURL.Name = "txtYTURL";
            this.txtYTURL.Size = new System.Drawing.Size(240, 20);
            this.txtYTURL.TabIndex = 3;
            // 
            // txtYTTile
            // 
            this.txtYTTile.Location = new System.Drawing.Point(52, 25);
            this.txtYTTile.Name = "txtYTTile";
            this.txtYTTile.Size = new System.Drawing.Size(240, 20);
            this.txtYTTile.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "URL: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Title: ";
            // 
            // gBoxIP
            // 
            this.gBoxIP.BackColor = System.Drawing.Color.Transparent;
            this.gBoxIP.Controls.Add(this.txtIPIp);
            this.gBoxIP.Controls.Add(this.txtIPTitle);
            this.gBoxIP.Controls.Add(this.label4);
            this.gBoxIP.Controls.Add(this.label5);
            this.gBoxIP.Location = new System.Drawing.Point(12, 58);
            this.gBoxIP.Name = "gBoxIP";
            this.gBoxIP.Size = new System.Drawing.Size(385, 100);
            this.gBoxIP.TabIndex = 5;
            this.gBoxIP.TabStop = false;
            this.gBoxIP.Text = "IP";
            // 
            // txtIPIp
            // 
            this.txtIPIp.Location = new System.Drawing.Point(52, 56);
            this.txtIPIp.Name = "txtIPIp";
            this.txtIPIp.Size = new System.Drawing.Size(240, 20);
            this.txtIPIp.TabIndex = 7;
            // 
            // txtIPTitle
            // 
            this.txtIPTitle.Location = new System.Drawing.Point(52, 25);
            this.txtIPTitle.Name = "txtIPTitle";
            this.txtIPTitle.Size = new System.Drawing.Size(240, 20);
            this.txtIPTitle.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "IP: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Title: ";
            // 
            // gBoxLF
            // 
            this.gBoxLF.BackColor = System.Drawing.Color.Transparent;
            this.gBoxLF.Controls.Add(this.label7);
            this.gBoxLF.Controls.Add(this.btnLFBrowse);
            this.gBoxLF.Controls.Add(this.txtLFPath);
            this.gBoxLF.Controls.Add(this.txtLFTitle);
            this.gBoxLF.Controls.Add(this.label6);
            this.gBoxLF.Location = new System.Drawing.Point(12, 58);
            this.gBoxLF.Name = "gBoxLF";
            this.gBoxLF.Size = new System.Drawing.Size(385, 100);
            this.gBoxLF.TabIndex = 4;
            this.gBoxLF.TabStop = false;
            this.gBoxLF.Text = "Local File";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 16);
            this.label7.TabIndex = 8;
            this.label7.Text = "Title: ";
            // 
            // btnLFBrowse
            // 
            this.btnLFBrowse.Location = new System.Drawing.Point(298, 54);
            this.btnLFBrowse.Name = "btnLFBrowse";
            this.btnLFBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnLFBrowse.TabIndex = 12;
            this.btnLFBrowse.Text = "Browse";
            this.btnLFBrowse.UseVisualStyleBackColor = true;
            this.btnLFBrowse.Click += new System.EventHandler(this.btnLFBrowse_Click);
            // 
            // txtLFPath
            // 
            this.txtLFPath.Location = new System.Drawing.Point(52, 56);
            this.txtLFPath.Name = "txtLFPath";
            this.txtLFPath.Size = new System.Drawing.Size(240, 20);
            this.txtLFPath.TabIndex = 11;
            // 
            // txtLFTitle
            // 
            this.txtLFTitle.Location = new System.Drawing.Point(52, 25);
            this.txtLFTitle.Name = "txtLFTitle";
            this.txtLFTitle.Size = new System.Drawing.Size(240, 20);
            this.txtLFTitle.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Path: ";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Gainsboro;
            this.btnCancel.Location = new System.Drawing.Point(53, 179);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.BackColor = System.Drawing.Color.Gainsboro;
            this.btnConfirm.Location = new System.Drawing.Point(259, 179);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(75, 23);
            this.btnConfirm.TabIndex = 6;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.UseVisualStyleBackColor = false;
            this.btnConfirm.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(409, 212);
            this.Controls.Add(this.gBoxIP);
            this.Controls.Add(this.gBoxYT);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.gBoxLF);
            this.Controls.Add(this.lblSource);
            this.Controls.Add(this.cbType);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Source";
            this.gBoxYT.ResumeLayout(false);
            this.gBoxYT.PerformLayout();
            this.gBoxIP.ResumeLayout(false);
            this.gBoxIP.PerformLayout();
            this.gBoxLF.ResumeLayout(false);
            this.gBoxLF.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.Label lblSource;
        private System.Windows.Forms.GroupBox gBoxYT;
        private System.Windows.Forms.TextBox txtYTURL;
        private System.Windows.Forms.TextBox txtYTTile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gBoxLF;
        private System.Windows.Forms.GroupBox gBoxIP;
        private System.Windows.Forms.TextBox txtIPIp;
        private System.Windows.Forms.TextBox txtIPTitle;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnLFBrowse;
        private System.Windows.Forms.TextBox txtLFPath;
        private System.Windows.Forms.TextBox txtLFTitle;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnConfirm;
    }
}