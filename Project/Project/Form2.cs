﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    public partial class Form2 : Form
    {
        public ImportFile FileToImport { get; set; }

        public bool safe {get; set;}


        public Form2(bool @checked, bool addingVideo)
        {
            InitializeComponent();

            

            if (@checked)
            {
                setDarkTheme();
            }

            if (addingVideo)
            {
                onLoadVideo();
            }

            onLoadDefault();

        }

        private void onLoadVideo()
        {
            cbType.Items.Remove("IP");
        }

        private void setDarkTheme()
        {
            this.BackColor = Color.FromArgb(12, 12, 12);


            lblSource.ForeColor = Color.White;
            cbType.BackColor = Color.FromArgb(52, 52, 52);
            cbType.ForeColor = Color.White;


            gBoxLF.BackColor = Color.FromArgb(32, 32, 32);
            gBoxLF.ForeColor = Color.FromArgb(152, 152, 152);
            txtLFPath.BackColor = Color.FromArgb(201,201,201);
            txtLFTitle.BackColor = Color.FromArgb(201,201,201);
            btnLFBrowse.ForeColor = Color.Black;


            gBoxIP.BackColor = Color.FromArgb(32, 32, 32);
            gBoxIP.ForeColor = Color.FromArgb(152, 152, 152);
            txtIPIp.BackColor = Color.FromArgb(201, 201, 201);
            txtIPTitle.BackColor = Color.FromArgb(201, 201, 201);

            gBoxYT.BackColor = Color.FromArgb(32, 32, 32);
            gBoxYT.ForeColor = Color.FromArgb(152, 152, 152);
            txtYTTile.BackColor = Color.FromArgb(201, 201, 201);
            txtYTURL.BackColor = Color.FromArgb(201, 201, 201);
        }

        private void onLoadDefault()
        {
            //Local File
            cbType.SelectedIndex = 1;

            txtIPTitle.MaxLength = 15;

            txtLFTitle.MaxLength = 15;

            txtYTTile.MaxLength = 15;

            gBoxIP.Hide();
            gBoxYT.Hide();

            safe = false;
        }

        private void cbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            clearTextBox();

            if (cbType.SelectedIndex == 1)          //LocalFile
            {
                gBoxLF.Show();

                gBoxYT.Hide();
                gBoxIP.Hide();
            }
            else if (cbType.SelectedIndex == 2)     //IP
            {
                gBoxIP.Show();

                gBoxLF.Hide();
                gBoxYT.Hide();
            }
            else                                    //YouTube
            {
                gBoxYT.Show();

                gBoxLF.Hide();
                gBoxIP.Hide();
            }
        }

        private void clearTextBox()
        {
            txtIPIp.Text = "";
            txtIPTitle.Text = "";
            txtLFPath.Text = "";
            txtLFTitle.Text = "";
            txtYTTile.Text = "";
            txtYTURL.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (cbType.SelectedIndex == 1)           //Local File
            {
                if (txtLFTitle.Text.Length > 0 && txtLFPath.Text.Length > 0)
                {
                    FileToImport = new ImportFile(txtLFTitle.Text, cbType.SelectedItem.ToString(), txtLFPath.Text);

                    safe = true;
                    this.Close();
                }
            }
            else if (cbType.SelectedIndex == 2)     //IP
            {
                if (txtIPTitle.Text.Length > 0 && txtIPIp.Text.Length > 0)
                {
                    FileToImport = new ImportFile(txtIPTitle.Text, cbType.SelectedItem.ToString(), txtIPIp.Text);

                    safe = true;
                    this.Close();
                }
            }
            else                                    //YouTube
            {
                if (txtYTTile.Text.Length > 0 && txtYTURL.Text.Length > 0)
                {
                    FileToImport = new ImportFile(txtYTTile.Text, cbType.SelectedItem.ToString(), txtYTURL.Text);

                    safe = true;
                    this.Close();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLFBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtLFPath.Text = dialog.FileName;
            }
        }
    }
}
