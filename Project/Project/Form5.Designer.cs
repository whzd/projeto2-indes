﻿namespace Project
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gBoxYouTube = new System.Windows.Forms.GroupBox();
            this.txtKeyYouTube = new System.Windows.Forms.TextBox();
            this.txtRTMPYouTube = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkBoxYouTube = new System.Windows.Forms.CheckBox();
            this.chkBoxTwitch = new System.Windows.Forms.CheckBox();
            this.gBoxTwitch = new System.Windows.Forms.GroupBox();
            this.txtKeyTwitch = new System.Windows.Forms.TextBox();
            this.txtRTMPTwitch = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chkBoxMixer = new System.Windows.Forms.CheckBox();
            this.gBoxMixer = new System.Windows.Forms.GroupBox();
            this.txtKeyMixer = new System.Windows.Forms.TextBox();
            this.txtRTMPMixer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gBoxYouTube.SuspendLayout();
            this.gBoxTwitch.SuspendLayout();
            this.gBoxMixer.SuspendLayout();
            this.SuspendLayout();
            // 
            // gBoxYouTube
            // 
            this.gBoxYouTube.BackColor = System.Drawing.Color.Transparent;
            this.gBoxYouTube.Controls.Add(this.txtKeyYouTube);
            this.gBoxYouTube.Controls.Add(this.txtRTMPYouTube);
            this.gBoxYouTube.Controls.Add(this.label2);
            this.gBoxYouTube.Controls.Add(this.label1);
            this.gBoxYouTube.Enabled = false;
            this.gBoxYouTube.Location = new System.Drawing.Point(160, 30);
            this.gBoxYouTube.Name = "gBoxYouTube";
            this.gBoxYouTube.Size = new System.Drawing.Size(366, 100);
            this.gBoxYouTube.TabIndex = 0;
            this.gBoxYouTube.TabStop = false;
            this.gBoxYouTube.Text = "YouTube";
            // 
            // txtKeyYouTube
            // 
            this.txtKeyYouTube.Location = new System.Drawing.Point(103, 58);
            this.txtKeyYouTube.Name = "txtKeyYouTube";
            this.txtKeyYouTube.Size = new System.Drawing.Size(257, 20);
            this.txtKeyYouTube.TabIndex = 3;
            // 
            // txtRTMPYouTube
            // 
            this.txtRTMPYouTube.Location = new System.Drawing.Point(103, 25);
            this.txtRTMPYouTube.Name = "txtRTMPYouTube";
            this.txtRTMPYouTube.Size = new System.Drawing.Size(257, 20);
            this.txtRTMPYouTube.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Stream Key:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "RTMP server:";
            // 
            // chkBoxYouTube
            // 
            this.chkBoxYouTube.AutoSize = true;
            this.chkBoxYouTube.BackColor = System.Drawing.Color.Transparent;
            this.chkBoxYouTube.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxYouTube.Location = new System.Drawing.Point(12, 67);
            this.chkBoxYouTube.Name = "chkBoxYouTube";
            this.chkBoxYouTube.Size = new System.Drawing.Size(93, 24);
            this.chkBoxYouTube.TabIndex = 0;
            this.chkBoxYouTube.Text = "YouTube";
            this.chkBoxYouTube.UseVisualStyleBackColor = false;
            this.chkBoxYouTube.CheckedChanged += new System.EventHandler(this.chkBoxYouTube_CheckedChanged);
            // 
            // chkBoxTwitch
            // 
            this.chkBoxTwitch.AutoSize = true;
            this.chkBoxTwitch.BackColor = System.Drawing.Color.Transparent;
            this.chkBoxTwitch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxTwitch.Location = new System.Drawing.Point(12, 201);
            this.chkBoxTwitch.Name = "chkBoxTwitch";
            this.chkBoxTwitch.Size = new System.Drawing.Size(73, 24);
            this.chkBoxTwitch.TabIndex = 1;
            this.chkBoxTwitch.Text = "Twitch";
            this.chkBoxTwitch.UseVisualStyleBackColor = false;
            this.chkBoxTwitch.CheckedChanged += new System.EventHandler(this.chkBoxTwitch_CheckedChanged);
            // 
            // gBoxTwitch
            // 
            this.gBoxTwitch.BackColor = System.Drawing.Color.Transparent;
            this.gBoxTwitch.Controls.Add(this.txtKeyTwitch);
            this.gBoxTwitch.Controls.Add(this.txtRTMPTwitch);
            this.gBoxTwitch.Controls.Add(this.label3);
            this.gBoxTwitch.Controls.Add(this.label4);
            this.gBoxTwitch.Enabled = false;
            this.gBoxTwitch.Location = new System.Drawing.Point(160, 163);
            this.gBoxTwitch.Name = "gBoxTwitch";
            this.gBoxTwitch.Size = new System.Drawing.Size(366, 100);
            this.gBoxTwitch.TabIndex = 4;
            this.gBoxTwitch.TabStop = false;
            this.gBoxTwitch.Text = "Twitch";
            // 
            // txtKeyTwitch
            // 
            this.txtKeyTwitch.Location = new System.Drawing.Point(103, 58);
            this.txtKeyTwitch.Name = "txtKeyTwitch";
            this.txtKeyTwitch.Size = new System.Drawing.Size(257, 20);
            this.txtKeyTwitch.TabIndex = 3;
            // 
            // txtRTMPTwitch
            // 
            this.txtRTMPTwitch.Location = new System.Drawing.Point(103, 25);
            this.txtRTMPTwitch.Name = "txtRTMPTwitch";
            this.txtRTMPTwitch.Size = new System.Drawing.Size(257, 20);
            this.txtRTMPTwitch.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Stream Key:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "RTMP server:";
            // 
            // chkBoxMixer
            // 
            this.chkBoxMixer.AutoSize = true;
            this.chkBoxMixer.BackColor = System.Drawing.Color.Transparent;
            this.chkBoxMixer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBoxMixer.Location = new System.Drawing.Point(12, 344);
            this.chkBoxMixer.Name = "chkBoxMixer";
            this.chkBoxMixer.Size = new System.Drawing.Size(65, 24);
            this.chkBoxMixer.TabIndex = 5;
            this.chkBoxMixer.Text = "Mixer";
            this.chkBoxMixer.UseVisualStyleBackColor = false;
            this.chkBoxMixer.CheckedChanged += new System.EventHandler(this.chkBoxMixer_CheckedChanged);
            // 
            // gBoxMixer
            // 
            this.gBoxMixer.BackColor = System.Drawing.Color.Transparent;
            this.gBoxMixer.Controls.Add(this.txtKeyMixer);
            this.gBoxMixer.Controls.Add(this.txtRTMPMixer);
            this.gBoxMixer.Controls.Add(this.label5);
            this.gBoxMixer.Controls.Add(this.label6);
            this.gBoxMixer.Enabled = false;
            this.gBoxMixer.Location = new System.Drawing.Point(160, 306);
            this.gBoxMixer.Name = "gBoxMixer";
            this.gBoxMixer.Size = new System.Drawing.Size(366, 100);
            this.gBoxMixer.TabIndex = 5;
            this.gBoxMixer.TabStop = false;
            this.gBoxMixer.Text = "Mixer";
            // 
            // txtKeyMixer
            // 
            this.txtKeyMixer.Location = new System.Drawing.Point(103, 58);
            this.txtKeyMixer.Name = "txtKeyMixer";
            this.txtKeyMixer.Size = new System.Drawing.Size(257, 20);
            this.txtKeyMixer.TabIndex = 3;
            // 
            // txtRTMPMixer
            // 
            this.txtRTMPMixer.Location = new System.Drawing.Point(103, 25);
            this.txtRTMPMixer.Name = "txtRTMPMixer";
            this.txtRTMPMixer.Size = new System.Drawing.Size(257, 20);
            this.txtRTMPMixer.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(17, 59);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 16);
            this.label5.TabIndex = 1;
            this.label5.Text = "Stream Key:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "RTMP server:";
            // 
            // btnConfirm
            // 
            this.btnConfirm.BackColor = System.Drawing.Color.Gainsboro;
            this.btnConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirm.Location = new System.Drawing.Point(63, 429);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(75, 23);
            this.btnConfirm.TabIndex = 6;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.UseVisualStyleBackColor = false;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Gainsboro;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(385, 429);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(539, 475);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.gBoxMixer);
            this.Controls.Add(this.chkBoxMixer);
            this.Controls.Add(this.gBoxTwitch);
            this.Controls.Add(this.chkBoxTwitch);
            this.Controls.Add(this.chkBoxYouTube);
            this.Controls.Add(this.gBoxYouTube);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form5";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Livestream Configuration";
            this.gBoxYouTube.ResumeLayout(false);
            this.gBoxYouTube.PerformLayout();
            this.gBoxTwitch.ResumeLayout(false);
            this.gBoxTwitch.PerformLayout();
            this.gBoxMixer.ResumeLayout(false);
            this.gBoxMixer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gBoxYouTube;
        private System.Windows.Forms.CheckBox chkBoxYouTube;
        private System.Windows.Forms.TextBox txtKeyYouTube;
        private System.Windows.Forms.TextBox txtRTMPYouTube;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkBoxTwitch;
        private System.Windows.Forms.GroupBox gBoxTwitch;
        private System.Windows.Forms.TextBox txtKeyTwitch;
        private System.Windows.Forms.TextBox txtRTMPTwitch;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkBoxMixer;
        private System.Windows.Forms.GroupBox gBoxMixer;
        private System.Windows.Forms.TextBox txtKeyMixer;
        private System.Windows.Forms.TextBox txtRTMPMixer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Button btnCancel;
    }
}