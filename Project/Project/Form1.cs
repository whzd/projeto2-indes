﻿using AxAXVLC;
using Emgu.CV;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Accord.Video.DirectShow;
using Emgu.CV.Structure;

namespace Project
{
    public partial class Form1 : Form
    {
        private static int MAX_SOURCE = 16;

        private static int MAX_VIDEOS = 20;

        private static int MAX_CAMERAS = 4;

        private Capture[] capture = new Capture[MAX_CAMERAS];

        private bool[] isCameraOn = { true, true, true, true };

        private bool isCameraLive;

        private int selectedLiveCamera = -1;

        private int nCameras;

        private AxVLCPlugin2[] previews;

        private ImportFile[] sources = new ImportFile[MAX_SOURCE];

        private int sourcesCont = 0;

        private Button[] previewButtons;

        private int activeLivePreview = -1;

        private bool isPopUp = false;

        private bool isAddSource = false;

        private Form3 livePopUP = null;

        private Form5 configLiveStream = null;

        private bool isStreamConfiged = false;

        private ImportFile[] playlistVideo = new ImportFile[MAX_VIDEOS];

        private int playlistCont = 0;

        private bool logoSelected = false;

        private string logoPath = "";

        private bool addingVideo = false;

        public Form1()
        {
            InitializeComponent();
            previewButtons = new Button[] { btnPrev1, btnPrev2, btnPrev3, btnPrev4, btnPrev5, btnPrev6, btnPrev7, btnPrev8, btnPrev9, btnPrev10, btnPrev11, btnPrev12, btnPrev13, btnPrev14, btnPrev15, btnPrev16, btnPlaylist };
            previews = new AxVLCPlugin2[] { vlc1, vlc2, vlc3, vlc4, vlc5, vlc6, vlc7, vlc8, vlc9, vlc10, vlc11, vlc12, vlc13, vlc14, vlc15, vlc16};
            onLoad();
        }

        private void SetButtonColor(Button button, bool condition)
        {
            button.BackColor = condition ? Color.Lime : Color.Red;
        }

        private void SetCameraColor(Button button, bool condition)
        {
            // button.BackColor = condition ? Color.Orange : Color.Red;
            button.BackColor = condition ? Color.Lime : Color.Red;
        }

        private void SetPreviewButtonColors()
        {
            for(int i = 0; i < previewButtons.Length; i++)
            {
                SetButtonColor(previewButtons[i], activeLivePreview == i);
            }
        }

        private void EnableCamera()
        {
            lstCamera.Items.Clear();
            FilterInfoCollection videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            nCameras = videoDevices.Count;
            capture = new Capture[MAX_CAMERAS];
            for(int i = 0; i < nCameras; i++)
            {
                lstCamera.Items.Add("Camera " + (i + 1), i);
            }
            for(int i = 0; i < MAX_CAMERAS; i++)
            {
                capture[i] = new Capture(i);
            }

            Application.Idle += new EventHandler(delegate (object sender, EventArgs e)
            {
                if (isCameraOn[0])
                {
                    imageBox1.Image = capture[0].QueryFrame();
                }
                else
                {
                    imageBox1.Image = null;
                }
                if (isCameraOn[1])
                {
                    imageBox2.Image = capture[1].QueryFrame();
                }
                else
                {
                    imageBox2.Image = null;
                }
                if (isCameraOn[2])
                {
                    imageBox3.Image = capture[2].QueryFrame();
                }
                else
                {
                    imageBox3.Image = null;
                }
                if (isCameraOn[3])
                {
                    imageBox4.Image = capture[3].QueryFrame();
                }
                else
                {
                    imageBox4.Image = null;
                }
                if(isCameraLive)
                {
                    try
                    {
                        imageBox5.Image = capture[selectedLiveCamera].QueryFrame();
                    }
                    catch
                    {
                        Image<Gray, Byte> img1 = new Image<Gray, Byte>(400, 300, new Gray(30));
                        imageBox5.Image = new Image<Bgr, Byte>(new Bitmap(Project.Properties.Resources.offline2));
                    }
                }
            });
        }

        private void redCameras()
        {
            SetCameraColor(btnCam1, false);
            SetCameraColor(btnCam2, false);
            SetCameraColor(btnCam3, false);
            SetCameraColor(btnCam4, false);
        }
        private void onLoad()
        {
            EnableCamera();
            redCameras();
            SetPreviewButtonColors();

            hideVLC();
           
            setListViewSingleColumn(lstSource);
            setListViewSingleColumn(lstPlaylist);
            setListViewSingleColumn(lstCamera);


            // Orelhas Quentes - https://youtu.be/i3hZlujcJg8
            // Witcher Trailer - https://youtu.be/ndl1W4ltcmg
            // Top Upcoming Action Movies Trailer - https://youtu.be/iO4MaSmuxTs
            // Top Upcoming Comedy Movies Trailer - https://youtu.be/qGft4Oe2nCE


            sources[sourcesCont] = new ImportFile("musica1", "YouTube", "https://youtu.be/i3hZlujcJg8");
            lstSource.Items.Add(sources[sourcesCont].Title, sourcesCont);
            sourcesCont++;

            vlc1.playlist.add("https://youtu.be/i3hZlujcJg8");
            vlc1.playlist.play();
            //vlc1.audio.volume = 50;
            vlc1.audio.mute = true;
            vlc1.Show();

        }

        private void hideVLC()
        {
            for (int i = 0; i < previews.Length ; i++)
            {
                previews[i].Hide();
            }

            vlcLive.Hide();
        }

        private void setListViewSingleColumn(ListView lst)
        {
            // single column
            // -2 => autosize
            lst.Columns.Add("Titulo", -2, HorizontalAlignment.Left);
            lst.Columns[0].Width = lst.Width - 4;
            lst.View = View.List;
            //listView1.HeaderStyle = ColumnHeaderStyle.None;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            activeLivePreview = 0;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev2_Click(object sender, EventArgs e)
        {
            activeLivePreview = 1;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev3_Click(object sender, EventArgs e)
        {
            activeLivePreview = 2;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev4_Click(object sender, EventArgs e)
        {
            activeLivePreview = 3;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev5_Click(object sender, EventArgs e)
        {
            activeLivePreview = 4;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev6_Click(object sender, EventArgs e)
        {
            activeLivePreview = 5;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev7_Click(object sender, EventArgs e)
        {
            activeLivePreview = 6;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev8_Click(object sender, EventArgs e)
        {
            activeLivePreview = 7;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev9_Click(object sender, EventArgs e)
        {
            activeLivePreview = 8;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev10_Click(object sender, EventArgs e)
        {
            activeLivePreview = 9;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev11_Click(object sender, EventArgs e)
        {
            activeLivePreview = 10;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev12_Click(object sender, EventArgs e)
        {
            activeLivePreview = 11;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev13_Click(object sender, EventArgs e)
        {
            activeLivePreview = 12;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev14_Click(object sender, EventArgs e)
        {
            activeLivePreview = 13;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev15_Click(object sender, EventArgs e)
        {
            activeLivePreview = 14;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPrev16_Click(object sender, EventArgs e)
        {
            activeLivePreview = 15;
            previewToLive(activeLivePreview);
            SetPreviewButtonColors();
        }

        private void btnPlaylist_Click(object sender, EventArgs e)
        {
            activeLivePreview = 16;
            SetPreviewButtonColors();
            isCameraLive = false;
            redCameras();
            imageBox5.Hide();
            vlcLive.Show();

            if (isPopUp)
            {
                livePopUP.playPlayList(playlistVideo, playlistCont);
            }
            if (vlcLive.playlist.itemCount > 0)
            {
                vlcLive.playlist.stop();
                vlcLive.playlist.items.clear();
            }
            for (int i = 0; i < playlistCont; i++)
            {
                vlcLive.playlist.add(playlistVideo[i].Path);
            }
            vlcLive.audio.volume = 0;
            vlcLive.playlist.play();
        }

        private void previewToLive(int sourcePos)
        {
            isCameraLive = false;
            redCameras();
            imageBox5.Hide();
            vlcLive.Show();
            if (isPopUp)
            {
                livePopUP.playVideo(sources[sourcePos].Path);
            }
            if (vlcLive.playlist.itemCount > 0)
            {
                vlcLive.playlist.stop();
                vlcLive.playlist.items.clear();
            }
            try
            {
                vlcLive.playlist.add(sources[sourcePos].Path);
            }
            catch
            {
                activeLivePreview = -1;
            }
            vlcLive.audio.volume = 0;
            vlcLive.playlist.play();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!isAddSource)
            {
                if (sourcesCont<MAX_SOURCE)
                {
                    Form2 importSouce = new Form2(themeDark.Checked, addingVideo);
                    importSouce.FormClosing += new FormClosingEventHandler(Form2_FormClosing);
                    importSouce.FormClosed += new FormClosedEventHandler(Form2_FormClosed);
                    importSouce.Show();

                    isAddSource = true;
                }
                else
                {
                    MessageBox.Show("You need to remove a video first.", "Limite Reached", MessageBoxButtons.OK);
                }
            }
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            isAddSource = false;
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((sender as Form2).safe)
            {
                sources[sourcesCont] = (sender as Form2).FileToImport;
                lstSource.Items.Add(sources[sourcesCont].Title, sourcesCont);
                previews[sourcesCont].playlist.add(sources[sourcesCont].Path);
                previews[sourcesCont].playlist.play();
                previews[sourcesCont].audio.mute = true;
                previews[sourcesCont].Show();
                sourcesCont++;
            }
        }

        private void cameraLive(int camera)
        {
            if(camera == selectedLiveCamera)
            {
                selectedLiveCamera = -1;
                redCameras();
                vlcLive.Show();
                imageBox5.Hide();
                isCameraLive = false;
                return;
            }
            selectedLiveCamera = camera;
            SetCameraColor(btnCam1, camera == 0);
            SetCameraColor(btnCam2, camera == 1);
            SetCameraColor(btnCam3, camera == 2);
            SetCameraColor(btnCam4, camera == 3);
            vlcLive.Hide();
            imageBox5.Show();
            vlcLive.playlist.stop();
            vlcLive.playlist.items.clear();
            activeLivePreview = -1;
            SetPreviewButtonColors();
            isCameraLive = true;
        }
        private void Button3_Click(object sender, EventArgs e)
        {
            if (nCameras < 1) return;
            // isCameraOn[0] = !isCameraOn[0];
            cameraLive(0);
        }

        private void btnPopUp_Click(object sender, EventArgs e)
        {
            if (!isPopUp)
            {

                if (vlcLive.playlist.isPlaying)
                {
                    Thread.Sleep(300);

                    livePopUP = new Form3(themeDark.Checked);
                    livePopUP.FormClosed += new FormClosedEventHandler(Form3_FormClosed);
                    if (activeLivePreview == 16)
                    {
                        livePopUP.playPlayList(playlistVideo, playlistCont);
                    }
                    else
                    {
                        livePopUP.playVideo(sources[activeLivePreview].Path);
                    }
                    if (logoSelected)
                    {
                        livePopUP.changeLogo(logoPath);
                    }
                    livePopUP.Show();

                    isPopUp = true;

                }
            }
        }

        private void Form3_FormClosed(object sender, FormClosedEventArgs e)
        {
            isPopUp = false;
        }

        private void btnLogo_Click(object sender, EventArgs e)
        {
            vlcLive.video.logo.enable();
            vlcLive.video.logo.position = "top-right";
            vlcLive.video.logo.opacity = 255;

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select Image";
            ofd.Filter = "Image (*.png) | *.png"; //Here you can filter which all files you wanted allow to open  
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                logoSelected = true;
                logoPath = ofd.FileName;

                vlcLive.video.logo.file(logoPath);

                if (livePopUP != null)
                {
                    livePopUP.changeLogo(logoPath);
                }
            }
        }

        private void btnSourceToPlayList_Click(object sender, EventArgs e)
        {
            if (lstSource.SelectedItems.Count > 0)
            {
                if (lstPlaylist.Items.Count < MAX_VIDEOS)
                {
                    if (!playlistVideo.Contains(sources[lstSource.SelectedIndices[0]]))
                    {
                        playlistVideo[playlistCont] = sources[lstSource.SelectedIndices[0]];
                        lstPlaylist.Items.Add(playlistVideo[playlistCont].Title, playlistCont);

                        playlistCont++;
                    }
                }
            }
        }

        private void btnPlaylistToSource_Click(object sender, EventArgs e)
        {
            if (lstPlaylist.SelectedItems.Count > 0)
            {
                if (lstSource.Items.Count < MAX_SOURCE)
                {
                    if (!sources.Contains(playlistVideo[lstPlaylist.SelectedIndices[0]]))
                    {
                        sources[sourcesCont] = playlistVideo[lstPlaylist.SelectedIndices[0]];
                        lstSource.Items.Add(sources[sourcesCont].Title, sourcesCont);

                        previews[sourcesCont].playlist.add(sources[sourcesCont].Path);
                        previews[sourcesCont].audio.volume = 0;
                        previews[sourcesCont].playlist.play();

                        sourcesCont++;
                    }
                }
            }
        }

        private void themeDark_Click(object sender, EventArgs e)
        {
            if (!themeDark.Checked)
            {
                themeLight.Checked = false;

                themeDark.Checked = true;
                setDarkTheme();
                if (isPopUp)
                {
                    livePopUP.setDarkTheme();
                }
            }
        }

        private void setDarkTheme()
        {
            this.BackColor = Color.FromArgb(12, 12, 12);

            menuStrip1.BackColor = Color.FromArgb(13, 13, 13);
            menuStrip1.ForeColor = Color.FromArgb(152, 152, 152);

            titleToolStripMenuItem.BackColor = Color.FromArgb(52, 52, 52);
            titleToolStripMenuItem.ForeColor = Color.FromArgb(152, 152, 152);

            mainLogoToolStripMenuItem.BackColor = Color.FromArgb(52, 52, 52);
            mainLogoToolStripMenuItem.ForeColor = Color.FromArgb(152, 152, 152);

            menuItemDark.BackColor = Color.FromArgb(52, 52, 52);
            menuItemDark.ForeColor = Color.FromArgb(152, 152, 152);

            themeLight.BackColor = Color.FromArgb(52, 52, 52);
            themeLight.ForeColor = Color.FromArgb(152, 152, 152);

            themeDark.BackColor = Color.FromArgb(52, 52, 52);
            themeDark.ForeColor = Color.FromArgb(152, 152, 152);

            configurationToolStripMenuItem.BackColor = Color.FromArgb(52, 52, 52);
            configurationToolStripMenuItem.ForeColor = Color.FromArgb(152, 152, 152);


            lblTitle.ForeColor = Color.White;


            gBoxPreview.BackColor = Color.FromArgb(32, 32, 32);
            gBoxPreview.ForeColor = Color.FromArgb(152, 152, 152);


            gBoxSource.BackColor = Color.FromArgb(32, 32, 32);
            gBoxSource.ForeColor = Color.FromArgb(152, 152, 152);

            lstSource.BackColor = Color.FromArgb(52, 52, 52);
            lstSource.ForeColor = Color.White;

            btnAddSource.ForeColor = Color.Black;
            btnRemoveSource.ForeColor = Color.Black;


            gBoxPlaylist.BackColor = Color.FromArgb(32, 32, 32);
            gBoxPlaylist.ForeColor = Color.FromArgb(152, 152, 152);

            lstPlaylist.BackColor = Color.FromArgb(52, 52, 52);
            lstPlaylist.ForeColor = Color.White;

            btnAddVideo.ForeColor = Color.Black;
            btnRemoveVideo.ForeColor = Color.Black;


            gBoxCamera.BackColor = Color.FromArgb(32, 32, 32);
            gBoxCamera.ForeColor = Color.FromArgb(152, 152, 152);


            gBoxCameraSource.BackColor = Color.FromArgb(32, 32, 32);
            gBoxCameraSource.ForeColor = Color.FromArgb(152, 152, 152);

            lstCamera.BackColor = Color.FromArgb(52, 52, 52);
            lstCamera.ForeColor = Color.White;

            button1.ForeColor = Color.Black;


            gBoxButtons.BackColor = Color.FromArgb(32, 32, 32);
            gBoxButtons.ForeColor = Color.FromArgb(152, 152, 152);

            btnPopUp.ForeColor = Color.Black;
            btnLogo.ForeColor = Color.Black;
            btnLive.ForeColor = Color.Black;
            button5.ForeColor = Color.Black;

            btnCam1.ForeColor = Color.Black;
            btnCam2.ForeColor = Color.Black;
            btnCam3.ForeColor = Color.Black;
            btnCam4.ForeColor = Color.Black;

            for (int i = 0; i < previewButtons.Length; i++)
            {
                previewButtons[i].ForeColor = Color.Black;
            }


            gBoxLive.BackColor = Color.FromArgb(32, 32, 32);
            gBoxLive.ForeColor = Color.FromArgb(152, 152, 152);

        }

        private void themeLight_Click(object sender, EventArgs e)
        {
            if (!themeLight.Checked)
            {
                themeDark.Checked = false;

                themeLight.Checked = true;
                setLightTheme();
                
            }
        }

        private void setLightTheme()
        {
            this.BackColor = Color.White;

            menuStrip1.BackColor = Color.White;
            menuStrip1.ForeColor = Color.Black;

            titleToolStripMenuItem.BackColor = Color.White;
            titleToolStripMenuItem.ForeColor = Color.Black;

            mainLogoToolStripMenuItem.BackColor = Color.White;
            mainLogoToolStripMenuItem.ForeColor = Color.Black;

            menuItemDark.BackColor = Color.White;
            menuItemDark.ForeColor = Color.Black;

            themeLight.BackColor = Color.White;
            themeLight.ForeColor = Color.Black;

            themeDark.BackColor = Color.White;
            themeDark.ForeColor = Color.Black;

            configurationToolStripMenuItem.BackColor = Color.White;
            configurationToolStripMenuItem.ForeColor = Color.Black;


            lblTitle.ForeColor = Color.Black;


            gBoxPreview.BackColor = Color.White;
            gBoxPreview.ForeColor = Color.Black;


            gBoxSource.BackColor = Color.White;
            gBoxSource.ForeColor = Color.Black;

            lstSource.BackColor = Color.White;
            lstSource.ForeColor = Color.Black;

            btnAddSource.ForeColor = Color.Black;
            btnRemoveSource.ForeColor = Color.Black;


            gBoxPlaylist.BackColor = Color.White;
            gBoxPlaylist.ForeColor = Color.Black;

            lstPlaylist.BackColor = Color.White;
            lstPlaylist.ForeColor = Color.Black;

            btnAddVideo.ForeColor = Color.Black;
            btnRemoveVideo.ForeColor = Color.Black;


            gBoxCamera.BackColor = Color.White;
            gBoxCamera.ForeColor = Color.Black;


            gBoxCameraSource.BackColor = Color.White;
            gBoxCameraSource.ForeColor = Color.Black;

            lstCamera.BackColor = Color.White;
            lstCamera.ForeColor = Color.Black;


            gBoxButtons.BackColor = Color.White;
            gBoxButtons.ForeColor = Color.Black;

            gBoxLive.BackColor = Color.White;
            gBoxLive.ForeColor = Color.Black;
        }

        private void titleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form4 setTitle = new Form4(themeDark.Checked);
            DialogResult dialogResult = setTitle.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                lblTitle.Text = setTitle.Title;
            }
        }

        private void mainLogoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Select Image";
            ofd.Filter = "Image (*.png) | *.png"; //Here you can filter which all files you wanted allow to open  
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                picBoxMainLogo.Image = Image.FromFile(ofd.FileName);
            }
        }

        private void configurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            configLiveStream = new Form5(themeDark.Checked);
            configLiveStream.FormClosing += new FormClosingEventHandler(Form5_FormClosing);
            configLiveStream.Show();
        }

        private void Form5_FormClosing(object sender, FormClosingEventArgs e)
        {
            isStreamConfiged = (sender as Form5).isConfigured;
        }

        private void btnLive_Click(object sender, EventArgs e)
        {
            if (!isStreamConfiged)
            {
                configLiveStream = new Form5(themeDark.Checked);
                configLiveStream.FormClosing += new FormClosingEventHandler(Form5_FormClosing);
                configLiveStream.Show();
            }
        }

        private void BtnCam2_Click(object sender, EventArgs e)
        {
            if (nCameras < 2) return;
            // isCameraOn[1] = !isCameraOn[1];
            cameraLive(1);
        }

        private void BtnCam3_Click(object sender, EventArgs e)
        {
            if (nCameras < 3) return;
            // isCameraOn[2] = !isCameraOn[2];
            cameraLive(2);
        }

        private void BtnCam4_Click(object sender, EventArgs e)
        {
            if (nCameras < 4) return;
            // isCameraOn[3] = !isCameraOn[3];
            cameraLive(3);
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            selectedLiveCamera = -1;
            vlcLive.Hide();
            imageBox5.Show();
            vlcLive.playlist.stop();
            vlcLive.playlist.items.clear();
            activeLivePreview = -1;
            SetPreviewButtonColors();
            isCameraLive = true;
            redCameras();
        }

        private void BtnRemoveSource_Click(object sender, EventArgs e)
        {
            lstSource.Items.Remove(lstSource.SelectedItems[0]);
        }

        private void BtnRemoveVideo_Click(object sender, EventArgs e)
        {
            //lstPlaylist.Items.Remove(lstPlaylist.SelectedItems[0]);
            lstPlaylist.Clear();
            playlistVideo = new ImportFile[MAX_VIDEOS];
            playlistCont = 0;
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            EnableCamera();
        }

        private void btnAddVideo_Click(object sender, EventArgs e)
        {
            if (sourcesCont < MAX_VIDEOS)
            {
                addingVideo = true;
                Form2 importVideo = new Form2(themeDark.Checked, addingVideo);
                importVideo.FormClosing += new FormClosingEventHandler(ImportVideo_FormClosing);
                importVideo.FormClosed += new FormClosedEventHandler(ImportVideo_FormClosed);
                importVideo.Show();
            }
            else
            {
                MessageBox.Show("You need to remove a video first.", "Limite Reached", MessageBoxButtons.OK);
            }
        }

        private void ImportVideo_FormClosed(object sender, FormClosedEventArgs e)
        {
            addingVideo = false;
        }

        private void ImportVideo_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((sender as Form2).safe)
            {
                playlistVideo[playlistCont] = (sender as Form2).FileToImport;
                lstPlaylist.Items.Add(playlistVideo[playlistCont].Title, playlistCont);
                playlistCont++;
            }
        }
    }
}
