﻿using AxAXVLC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    public partial class Form3 : Form
    {
        public AxVLCPlugin2 vlcNe { get; set; }

        public Form3(bool @checked)
        {
            InitializeComponent();

            if (@checked)
            {
                setDarkTheme();
            }
        }

        public void setDarkTheme()
        {
            this.BackColor = Color.FromArgb(12, 12, 12);
        }

        public void playVideo(string video)
        {
            if (vlcNewLive.playlist.itemCount > 0)
            {
                vlcNewLive.playlist.stop();
                vlcNewLive.playlist.items.clear();
            }
            vlcNewLive.playlist.add(video);
            vlcNewLive.audio.volume = 50;
            vlcNewLive.playlist.play();
        }

        public void changeLogo(string fileName)
        {
            vlcNewLive.video.logo.enable();
            vlcNewLive.video.logo.position = "top-right";
            vlcNewLive.video.logo.opacity = 255;
            vlcNewLive.video.logo.file(fileName);
        }

        internal void playPlayList(ImportFile[] playlist, int playlistCont)
        {
            if (vlcNewLive.playlist.itemCount > 0)
            {
                vlcNewLive.playlist.stop();
                vlcNewLive.playlist.items.clear();
            }
            for (int i = 0; i < playlistCont; i++)
            {
                vlcNewLive.playlist.add(playlist[i].Path);
            }
            vlcNewLive.audio.volume = 0;
            vlcNewLive.playlist.play();
        }
    }
}
