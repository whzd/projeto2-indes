﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    public partial class Form5 : Form
    {
        public string rtmpYouTube { get; set; }

        public string keyYouTube { get; set; }

        public string rtmpTwitch { get; set; }

        public string keyTwitch { get; set; }

        public string rtmpMixer { get; set; }

        public string keyMixer { get; set; }

        public bool isConfigured { get; set; }

        public Form5(bool @checked)
        {
            InitializeComponent();
            isConfigured = false;

            if (@checked)
            {
                setDarkTheme();
            }
        }

        private void setDarkTheme()
        {
            this.BackColor = Color.FromArgb(12, 12, 12);

            chkBoxYouTube.ForeColor = Color.White;
            chkBoxTwitch.ForeColor = Color.White;
            chkBoxMixer.ForeColor = Color.White;

            gBoxYouTube.BackColor = Color.FromArgb(32, 32, 32);
            gBoxYouTube.ForeColor = Color.FromArgb(152, 152, 152);
            txtRTMPYouTube.BackColor = Color.FromArgb(201, 201, 201);
            txtKeyYouTube.BackColor = Color.FromArgb(201, 201, 201);

            gBoxTwitch.BackColor = Color.FromArgb(32, 32, 32);
            gBoxTwitch.ForeColor = Color.FromArgb(152, 152, 152);
            txtRTMPTwitch.BackColor = Color.FromArgb(201, 201, 201);
            txtKeyTwitch.BackColor = Color.FromArgb(201, 201, 201);

            gBoxMixer.BackColor = Color.FromArgb(32, 32, 32);
            gBoxMixer.ForeColor = Color.FromArgb(152, 152, 152);
            txtRTMPMixer.BackColor = Color.FromArgb(201, 201, 201);
            txtKeyMixer.BackColor = Color.FromArgb(201, 201, 201);
        }

        private void chkBoxYouTube_CheckedChanged(object sender, EventArgs e)
        {
            gBoxYouTube.Enabled = !gBoxYouTube.Enabled;
        }

        private void chkBoxTwitch_CheckedChanged(object sender, EventArgs e)
        {
            gBoxTwitch.Enabled = !gBoxTwitch.Enabled;
        }

        private void chkBoxMixer_CheckedChanged(object sender, EventArgs e)
        {
            gBoxMixer.Enabled = !gBoxMixer.Enabled;
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (gBoxYouTube.Enabled || gBoxTwitch.Enabled || gBoxMixer.Enabled)
            {
                if (gBoxYouTube.Enabled)
                {
                    if (txtRTMPYouTube.Text.Length > 0 && txtKeyYouTube.Text.Length >0)
                    {
                        rtmpYouTube = txtRTMPYouTube.Text;
                        keyYouTube = txtKeyYouTube.Text;

                        isConfigured = true;
                    }
                }

                if (gBoxTwitch.Enabled)
                {
                    if (txtRTMPTwitch.Text.Length > 0 && txtRTMPTwitch.Text.Length > 0)
                    {
                        rtmpTwitch = txtRTMPTwitch.Text;
                        keyTwitch = txtRTMPTwitch.Text;

                        isConfigured = true;
                    }
                }

                if (gBoxMixer.Enabled)
                {
                    if (txtRTMPMixer.Text.Length > 0 && txtKeyMixer.Text.Length > 0)
                    {
                        rtmpMixer = txtRTMPMixer.Text;
                        keyMixer = txtKeyMixer.Text;

                        isConfigured = true;
                    }
                }

                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
