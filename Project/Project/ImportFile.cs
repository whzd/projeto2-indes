﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project
{
    public class ImportFile
    {
        public string Title { get; set; }
        public string Type { get; set; }
        public string Path { get; set; }

        public ImportFile(string Title, string Type, string Path)
        {
            this.Title = Title;
            this.Type = Type;
            this.Path = new Uri(Path).AbsoluteUri;
        }
        public override string ToString()
        {
            return Title;
        }

    }
}
