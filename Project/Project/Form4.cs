﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project
{
    public partial class Form4 : Form
    {
        public Form4(bool @checked)
        {
            InitializeComponent();

            if (@checked)
            {
                setDarkTheme();
            }
        }

        private void setDarkTheme()
        {
            this.BackColor = Color.FromArgb(12, 12, 12);

            label1.ForeColor = Color.White;

            txtTitle.BackColor = Color.FromArgb(201, 201, 201);
        }

        public string Title
        {
            get { return txtTitle.Text; }
        }
    }
}
