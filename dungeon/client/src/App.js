import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from 'react-router-dom';
import swal from 'sweetalert';
import { mapList } from './mapList';

// Fisher-Yates Shuffle
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const secondsToMinutes = seconds => Math.floor(seconds / 60) + ':' + ('0' + Math.floor(seconds % 60)).slice(-2);

// clone game map to avoid mutating react state directly
// if the array contained primitive types slicing the lines would be enough
// however with objects we must clone them because of shared references
function cloneGameMap(game) {
  return game.map(line => {
    return line.map(column => {
      return column.clone();
    })
  });
}

function between(x, min, max) {
  return x >= min && x <= max;
}

function stringMapToMatrix(map, previousPlayer = null, enemiesLevel = 1, teleport = true,
  enemiesCount = 3, weaponsCount = 1, healthCount = 2) {

  let matrix = [];
  let emptySpots = [];
  let line = 0;
  let column = 0;
  matrix[line] = [];

  // load string to matrix and get empty spot positions, ignore first and last line
  for (let i = 1; i < map.length - 1; i++) {
    let ch = map.charAt(i);
    // empty spot found, add it to the empty spots array
    if (ch === PointTypes.EMPTY) {
      emptySpots.push({ x: line, y: column });
    }
    // add the point to the matrix
    matrix[line].push(new PointType(ch));
    column++;
    // newline found, create a new line in the matrix and reset column
    if (ch === PointTypes.NEWLINE) {
      matrix[++line] = [];
      column = 0;
    }
  }

  // shuffling the array is an easy way to add stuff in sequence at random positions without overlap
  // assume there are always more empty spots than enemies + weapons + healths + player + teleport
  shuffle(emptySpots);

  let counter = 0;
  let pos;

  // add enemies
  for (let i = 0; i < enemiesCount; i++) {
    pos = emptySpots[counter++];
    matrix[pos.x][pos.y] = new Enemy(enemiesLevel);
  }
  // add weapons
  for (let i = 0; i < weaponsCount; i++) {
    pos = emptySpots[counter++];
    matrix[pos.x][pos.y] = new PointType(PointTypes.WEAPON);
  }
  // add health pickups
  for (let i = 0; i < healthCount; i++) {
    pos = emptySpots[counter++];
    matrix[pos.x][pos.y] = new PointType(PointTypes.HEALTH);
  }
  // add player
  pos = emptySpots[counter++];
  let player;
  if (previousPlayer) {
    player = new Player(pos.x, pos.y, previousPlayer.health, previousPlayer.baseDamage, previousPlayer.level);
  }
  else {
    player = new Player(pos.x, pos.y);
  }
  matrix[pos.x][pos.y] = player;
  // add teleport
  if (teleport) {
    pos = emptySpots[counter++];
    matrix[pos.x][pos.y] = new PointType(PointTypes.TELEPORT);
  }

  return {
    map: matrix,
    player: player
  }
}

const PointTypes = {
  PLAYER: 'p',
  WALL: '#',
  EMPTY: ' ',
  NEWLINE: '\n',
  WEAPON: 'w',
  HEALTH: 'h',
  ENEMY: 'e',
  TELEPORT: 't',
  SHOP: 's',
  SHOPKEEPER: 'k',
  SHOPCOUNTER: 'c',
  BOSS1: '1',
  BOSS2: '2',
  BOSS3: '3',
  BOSS4: '4',
  BOSS5: '5',
  BOSS6: '6',
  BOSS7: '7',
  BOSS8: '8',
  BOSS9: '9',
  BOSS10: '0',
  BOSS11: 'n',
  BOSS12: 'm',
  BOSS13: 'z',
  BOSS14: 'x',
  BOSS15: 'd',
  BOSS16: 'f',
}

function getPointTypeLowercaseKey(value) {
  return Object.keys(PointTypes).find(key => PointTypes[key] === value).toLowerCase();
}

class PointType {
  constructor(type) {
    this.type = type;
  }

  clone() {
    return new PointType(this.type);
  }
}

class Being extends PointType {
  constructor(type, health, baseDamage, level) {
    super(type);
    this.health = health;
    this.baseDamage = baseDamage;
    this.level = level;
  }

  clone() {
    return new Being(this.type, this.health, this.baseDamage, this.level);
  }

  // damage = +- 2.5 + levelDifference + (baseDamage +- 10)
  inflictDamage(being) {
    const levelDiff = this.level - being.level;
    being.health -= Math.floor(levelDiff * 2.5 + randomInt(this.baseDamage - 10, this.baseDamage + 10));
  }

  isDead() {
    return this.health <= 0;
  }
}

class Player extends Being {
  constructor(x, y, health = 100, baseDamage = 30, level = 1) {
    super(PointTypes.PLAYER, health, baseDamage, level);
    this.x = x;
    this.y = y;
  }

  clone() {
    return new Player(this.x, this.y, this.health, this.baseDamage, this.level);
  }

  changePos(x, y) {
    this.x = x;
    this.y = y;
  }

  pickHealth() {
    this.health += randomInt(20, 60);
  }

  pickWeapon() {
    this.baseDamage += randomInt(5, 10);
  }

  increaseLevel() {
    this.level++;
  }

  enemiesDefeated() {
    return this.level - 1;
  }

}

class Enemy extends Being {
  constructor(level) {
    super(PointTypes.ENEMY, 100, 25, level);
  }
}

class GameApp extends React.Component {

  constructor(props) {
    super(props);

    this.state = this.loadMap(0);
    this.state.boss = new Enemy(this.props.maps.length + 3);
    this.state.isOver = false;
    this.state.showLeaderboard = false;
    this.state.currentMapIndex = 0;
    this.state.darkMode = true;
    this.state.screen = 'opening';
    this.state.seconds = 0;
    this.state.cellSize = {
      width: '25px',
      height: '25px',
      float: 'left'
    }
  }

  tick() {
    this.setState(state => ({
      seconds: state.seconds + 1
    }));
  }

  finishGame(isWin) {
    clearInterval(this.interval);
    this.setState({ isOver: true });

    if (!isWin) {
      return swal({
        title: 'You lost!',
        text: 'Do you want to play again?',
        buttons: ['No', 'Yes'],
      }).then(playAgain => {
        if (playAgain) {
          window.location.reload();
        }
      });
    }

    const score = Math.round(this.props.maxScore / (this.state.seconds / (this.state.player.enemiesDefeated() + 1)));
    swal({
      title: 'You win!',
      text: `Your score was ${score}.\nEnter your name to be in the scoreboard.`,
      icon: 'success',
      content: {
        element: 'input',
        attributes: {
          value: localStorage.getItem('name')
        }
      },
      buttons: ['Cancel', 'Ok'],
    }).then(name => {
      if (name) {
        fetch('http://localhost:4000', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({ player: name, score }),
        })
          .then(() => this.setState({ showLeaderboard: true }))
          .catch(() => swal({
            title: 'Server Error!',
            text: 'Unable to save your score to the leaderboard.',
            icon: 'error',
          }));
      }
    })

  }

  loadMap(index) {
    const difficulty = localStorage.getItem('difficulty');
    const enemiesDifficulty = {
      'easy': 2,
      'normal': 4,
      'hard': 6
    };
    const enemies = enemiesDifficulty[difficulty];
    // on first load it should create the player
    if (index === 0)
      return stringMapToMatrix(this.props.maps[index], null, 1, true, enemies);
    // last map is boss level
    if (index === this.props.maps.length - 1)
      return stringMapToMatrix(this.props.maps[index], this.state.player, 0, false, 0, 0, 1);
    // load current map and keep player state
    return stringMapToMatrix(this.props.maps[index], this.state.player, index + 1, true, enemies);
  }

  handleKeyDown(event) {
    if (this.state.isOver) return; // if game is over ignore input
    switch (event.keyCode) {
      case 37:
        this.movePlayer(this.state.player.x, this.state.player.y - 1);
        break;
      case 38:
        this.movePlayer(this.state.player.x - 1, this.state.player.y);
        break;
      case 39:
        this.movePlayer(this.state.player.x, this.state.player.y + 1);
        break;
      case 40:
        this.movePlayer(this.state.player.x + 1, this.state.player.y);
        break;
      case 69:

        if (this.state.player.x - 2 > 0 && this.state.map[this.state.player.x - 2][this.state.player.y].type === PointTypes.SHOPKEEPER) {
          this.shopping();

        } else if ([PointTypes.ENEMY, PointTypes.BOSS4, PointTypes.BOSS8,
        PointTypes.BOSS12, PointTypes.BOSS16].includes(this.state.map[this.state.player.x][this.state.player.y - 1].type)) {

          this.attack(this.state.player.x, this.state.player.y - 1);

        } else if ([PointTypes.ENEMY, PointTypes.BOSS13, PointTypes.BOSS14,
        PointTypes.BOSS15, PointTypes.BOSS16].includes(this.state.map[this.state.player.x - 1][this.state.player.y].type)) {

          this.attack(this.state.player.x - 1, this.state.player.y);

        } else if ([PointTypes.ENEMY, PointTypes.BOSS1, PointTypes.BOSS5,
        PointTypes.BOSS9, PointTypes.BOSS13].includes(this.state.map[this.state.player.x][this.state.player.y + 1].type)) {

          this.attack(this.state.player.x, this.state.player.y + 1);

        } else if ([PointTypes.ENEMY, PointTypes.BOSS1, PointTypes.BOSS2,
        PointTypes.BOSS3, PointTypes.BOSS4].includes(this.state.map[this.state.player.x + 1][this.state.player.y].type)) {

          this.attack(this.state.player.x + 1, this.state.player.y);
        }

        break;
      default:
        break;
    }
  }

  shopping() {

    let player = this.state.player.clone();

    if (player.health > 5) {
      player.health -= 5;
      player.baseDamage += randomInt(-5, 8);
      this.setState({ player: player });
    }

  }

  attack(x, y) {
    let player = this.state.player.clone();
    let boss = this.state.boss.clone();
    let map = cloneGameMap(this.state.map);

    const isBoss = map[x][y].type !== PointTypes.ENEMY;

    let enemy = isBoss ? boss : map[x][y];
    player.inflictDamage(enemy);
    if (enemy.isDead()) {
      if (!isBoss) {
        map[x][y] = new PointType(PointTypes.EMPTY);
      }
      player.increaseLevel();

      // check win (assumes last level has only 1 enemy)
      if (this.state.currentMapIndex === this.props.maps.length - 1) this.finishGame(true);
    }
    else {
      enemy.inflictDamage(player);
      if (player.isDead()) this.finishGame(false);
    }

    this.setState({ player: player, map: map, boss: boss });

  }

  movePlayer(x, y) {
    if (this.state.map[x][y].type === PointTypes.TELEPORT) {
      const mapInfo = this.loadMap(this.state.currentMapIndex + 1);
      this.setState({
        map: mapInfo.map,
        player: mapInfo.player,
        currentMapIndex: this.state.currentMapIndex + 1
      });
      return;
    }

    // clone objects to avoid changing state directly, there are more efficient ways
    let player = this.state.player.clone();
    let map = cloneGameMap(this.state.map);

    if (map[x][y].type === PointTypes.HEALTH) {
      player.pickHealth();
      map[x][y] = new PointType(PointTypes.EMPTY);
    }

    if (map[x][y].type === PointTypes.WEAPON) {
      player.pickWeapon();
      map[x][y] = new PointType(PointTypes.EMPTY);
    }

    if (map[x][y].type === PointTypes.EMPTY) {
      map[player.x][player.y] = new PointType(PointTypes.EMPTY);
      player.changePos(x, y);
      map[x][y] = player;
    }

    this.setState({ player: player, map: map });
  }

  calculateSizes() {
    const docs = document.getElementsByClassName('gameMap');
    if (docs.length === 0) return;
    const size = (document.getElementsByClassName('gameMap')[0].clientWidth - 20) /
      this.props.maps[this.state.currentMapIndex].split('\n')[1].length
    this.setState({
      cellSize: {
        width: size + 'px',
        height: size + 'px',
        float: 'left'
      }
    });
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
    document.addEventListener("keydown", this.handleKeyDown.bind(this));
    window.addEventListener("resize", this.calculateSizes.bind(this));
    this.calculateSizes();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    document.removeEventListener("keydown", this.handleKeyDown.bind(this));
    window.removeEventListener("resize", this.calculateSizes.bind(this));
  }

  render() {
    if (this.state.showLeaderboard) return <Redirect to='/leaderboard' />

    let result = [];
    let map = this.state.map;

    // for big maps, dont start at 0 and stop at map length, calculate based on player position and screen size
    for (let i = 0; i < map.length; i++) {
      for (let j = 0; j < map[i].length; j++) {
        let point = map[i][j];
        let pointId = i + ',' + j;

        if (this.state.darkMode) {
          // if dark mode is enabled and points are not newline or in range, draw it black
          if ((between(i, this.state.player.x - this.props.visibleRadius, this.state.player.x + this.props.visibleRadius)
            && between(j, this.state.player.y - this.props.visibleRadius, this.state.player.y + this.props.visibleRadius))
            || point.type === PointTypes.NEWLINE) {
            result.push(<div key={pointId} style={point.type === PointTypes.NEWLINE ? {} : this.state.cellSize}
              className={getPointTypeLowercaseKey(point.type)} />);
          }
          else {
            result.push(<div key={pointId} style={this.state.cellSize} className='dark' />);
          }
        }
        else {
          result.push(<div key={pointId} style={point.type === PointTypes.NEWLINE ? {} : this.state.cellSize}
            className={getPointTypeLowercaseKey(point.type)} />);
        }
      }
    }

    return (
      <div>
        <h1>Dungeon Crawler</h1>
        <div className="gameArea">
          <div className="gameInfo">
            <div>Health<VerticalBar color="#4CAF50" padding="10px" value={this.state.player.health} maxValue="200" /></div>
            <div>Power
              <VerticalBar color="#FF5722" padding="10px"
                value={Math.floor((this.state.player.level * 2.5) + this.state.player.baseDamage)}
                maxValue="100" />
            </div>
            <div>Enemies defeated: <b>{Math.floor(this.state.player.enemiesDefeated())}</b></div>
            <div>Time: <b>{secondsToMinutes(this.state.seconds)}</b></div>
            <div>Level: <b>{this.state.currentMapIndex + 1}/{this.props.maps.length}</b></div>
            <button className="btn" onClick={() => this.setState({ darkMode: !this.state.darkMode })}>Toggle dark</button>
            <button className="btn" onClick={() => window.location.reload()}>New game</button>
          </div>
          <div className="gameMap">
            {result}
            <br/>
            <p>Use arrow keys to move and press 'E' to attack or use the store. Find your way out of the dungeon!</p>
          </div>
        </div>
      </div>
    );
  }

}

function VerticalBar(props) {
  let width = props.value / props.maxValue * 100;
  const hide = width < 0;
  const style = {
    backgroundColor: hide ? 'red' : props.color,
    padding: props.padding,
    maxWidth: (hide ? 100 : width) + '%',
    color: '#FFF',
    textAlign: 'center'
  }
  return <div className="verticalBar" style={style}>{props.value}</div>;
}

function App() {
  return (
    <Router>
      <div>
        <div className="route-content">
          <Switch>
            <Route path="/game">
              <GameApp maps={mapList} visibleRadius={5} maxScore={999999} />
            </Route>
            <Route path="/leaderboard">
              <Leaderboard />
            </Route>
            <Route path="/manual">
              <Manual />
            </Route>
            <Route path="/settings">
              <Settings />
            </Route>
            <Route path="/credits">
              <Credits />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </div>

        <nav>
          <ul className="vertical-menu">
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/game">Game</Link>
            </li>
            <li>
              <Link to="/leaderboard">Leaderboard</Link>
            </li>
            <li>
              <Link to="/manual">Manual</Link>
            </li>
            <li>
              <Link to="/settings">Settings</Link>
            </li>
            <li>
              <Link to="/credits">Credits</Link>
            </li>
          </ul>
        </nav>

      </div>
    </Router>
  );
}

function Home() {
  return (
    <div>
      <h1>Project 3</h1>
      <p>This project was developed for the Interface and Design class.</p>
      <p>The goal of this project was to develop a <Link to="/game">game</Link>.</p>
      <p>If you are not familiar with dungeon crawler games check the <Link to="/manual">manual</Link>.</p>
    </div>
  );
}

class Leaderboard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      hasError: false,
      playerScores: []
    }
  }

  componentDidMount() {
    fetch('http://localhost:4000')
      .then(res => res.json())
      .then(res => this.setState({ playerScores: res }))
      .catch(err => {
        this.setState({ hasError: true });
        console.error(err);
      })
      .finally(() => this.setState({ isLoading: false }));
  }

  render() {
    return (
      <div className="leaderboard">
        <h1>Leaderboard</h1>

        {this.state.isLoading &&
          <p>Loading leaderboard...</p>
        }

        {this.state.hasError &&
          <p>Unable to load the leaderboard...</p>
        }

        {!this.state.isLoading && !this.state.hasError && this.state.playerScores.length === 0 &&
          <p>The leaderboard is empty... Now is your chance!</p>
        }

        {this.state.playerScores.length > 0 &&
          <table>
            <thead>
              <tr>
                <th>Position</th>
                <th>Name</th>
                <th>Points</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
              {this.state.playerScores.map((playerScore, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{playerScore.player}</td>
                    <td>{playerScore.score}</td>
                    <td>{new Date(playerScore.when).toLocaleString()}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        }
      </div>
    );
  }
}

function Manual() {
  return (
    <div>
      <h1>Manual</h1>
      <p>Like most Dungeon Crawler games, use the arrow keys to explore the dark map.</p>
      <p>Enemies spawn randomly, move towards them and press <b>E</b> to attack, however they will stike back.</p>
      <p>Catch hearts that spawn at random to gain hp. If you reach 0 you die.</p>
      <p>With increased power you deal more damage. Defeat enemies or catch powerups to improve.</p>
      <p>You can also gamble away health for power at shops, but be careful.</p>
      <p>To progress to the next level, find the portal. At the last level there is a boss fight.</p>
      <p>The objective of the game is to defeat as many enemies possible in the shortest time. The score will reflect this.</p>
      <p>After beating the game you can add your score to the <Link to="/leaderboard">leaderboard.</Link></p>
      <p>The difficulty (number of enemies) can be changed in the <Link to="/settings">settings</Link>.</p>
    </div>
  );
}

function Credits() {
  return (
    <div>
      <h1>Credits</h1>
      <p>This game was developed by:</p>
      <p>1140279 - André Dias</p>
      <p>1150727 - Rui Almeida</p>
      <p>1150828 - Miguel Carneiro</p>
    </div>
  );
}

class Settings extends React.Component {

  constructor(props) {
    super(props);
    this.state = { name: '', difficulty: 'normal' };

    this.handleChangeName = this.handleChangeName.bind(this);
    this.handleChangeDifficulty = this.handleChangeDifficulty.bind(this);
  }

  componentDidMount() {
    const name = localStorage.getItem('name');
    const difficulty = localStorage.getItem('difficulty');
    if(name) this.setState({ name });
    if(difficulty) this.setState({ difficulty });
  }

  handleChangeName(event) {
    const name = event.target.value;
    this.setState({ name });
    localStorage.setItem('name', name);
  }

  handleChangeDifficulty(event) {
    const difficulty = event.target.value;
    this.setState({ difficulty });
    localStorage.setItem('difficulty', difficulty);
  }

  render() {
    return (
      <div>
        <h1>Settings</h1>
        <div className="settings">
          <label htmlFor="playerName">Name</label>
          <input id="playerName" placeholder="Name for scoreboard" type="text" autoComplete="off"
            value={this.state.name} onChange={this.handleChangeName}></input>
        </div>
        <div className="settings">
          <label htmlFor="difficulty">Difficulty</label>
          <select id="difficulty" value={this.state.difficulty} onChange={this.handleChangeDifficulty}>
            <option value="easy">Easy</option>
            <option value="normal">Normal</option>
            <option value="hard">Hard</option>
          </select>
        </div>
      </div>
    );
  }
}


export default App;
