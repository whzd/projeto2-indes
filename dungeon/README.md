# INDES - Dungeon Crawler

## Game Overview

A Dungeon Crawler type of game was developed for the web browser using HTML, CSS and JavaScript.

The objective is to defeat as many enemies possible in the shortest time.

The player needs to explore a dark dungeon to find the way out using portals and defeat the last enemy.

Health/damage powerups and enemies spawn at random. Enemies stats improve in each level.

At shops the player can gamble away health for increased damage. Killing enemies also improves damage.

![game](images/game.png)

A footer is always presented to allow the user to easily navigate to all the different pages.

After beating the game, the score is presented and the user has the choice of entering his name to be added to a leaderboard.

![win](images/win.png)

## Manual

A manual page is available with all the information needed to play the game presented in a clear way.

![manual](images/manual.png)

## Leaderboard

To allow multiple players to compete, a leaderboard was added. The scores are saved server-side.

![leaderboard](images/leaderboard.png)

## Settings

The name and difficuly can be changed in a settings page.

With a name set, when beating the game, the textbox will be pre-filled with it.

![settings](images/settings.png)

