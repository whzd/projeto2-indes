const express = require('express');
const fs = require('fs');
const app = express();
const cors = require('cors')
const port = process.env.PORT || 4000;

const PLAYER_SCORES_FILE = './playerScores.json';

function loadPlayerScores(file = PLAYER_SCORES_FILE) {
  if(!fs.existsSync(file)) {
    return [];
  }
  return require(file);
}

function writePlayerScores(playerScores, file = PLAYER_SCORES_FILE) {
  fs.writeFileSync(file, JSON.stringify(playerScores, null, 2));
}

const playerScores = loadPlayerScores();

app.use(cors())
app.use(express.json());

app.get('/', (req, res) => {
  res.json(playerScores.sort((a, b) => b.score - a.score));
});

app.post('/', (req, res) => {
  const playerScore = {
    player: req.body.player,
    score: parseInt(req.body.score),
    when: new Date()
  };
  playerScores.push(playerScore);
  writePlayerScores(playerScores);
  res.json(playerScore);
});

app.listen(port, () => console.log(`App listening on port ${port}!`))
