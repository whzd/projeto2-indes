# Interfaces e Design

## Application overview

When the application starts the user is presented with the form in the following image.

![Main window](./img/main_window.png)

This is the main window. Here the user can upload videos that will be presented in the **Preview** section.

It is possible to create a custom playlist by moving videos from the **Source** section to the **Playlist**.

With the buttons to the left of the **Live** section, the user can send the corresponding preview to the live feed.

Other options include the ability to add a logo watermark, set a static offline image or open a new window with the live video.

![Popup](./img/livePopUp.png)

## Video sources

The user is allowed to use his own WebCams by pressing the **Camara** buttons.

When the user presses the **Add Source** button, a new form will apear.
This form changes according to the option chosen in the ComboBox, as it can be seen in the following three images.

![Add Local File](./img/add_localfile.png)

Allows the user to browse the computer for a video file.

![Add Youtube](./img/add_youtube.png)

Allows the user to add a YouTube video from a given URL.

![Add IP Camara](./img/add_ipcamara.png)

Allows the user to add an IP Camara from a given IP.

## Customization

The user can customize the title, main logo and theme.

![Config Options](./img/configOption.png)

![Set Title](./img/setTitle.png)

## Livestream

The user can open the Livestream dialog and enter the configuration details.

![Livestream](./img/configLiveStream.png)

![Config Livestream](./img/liveStreamConfig.png)

